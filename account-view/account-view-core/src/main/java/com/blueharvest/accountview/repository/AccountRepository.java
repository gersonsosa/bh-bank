package com.blueharvest.accountview.repository;

import com.blueharvest.account.model.Account;
import java.util.List;

public interface AccountRepository {

  void save(Account account);

  List<Account> findByCustomerId(String customerId);

}

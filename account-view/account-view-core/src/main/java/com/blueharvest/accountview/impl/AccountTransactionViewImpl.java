package com.blueharvest.accountview.impl;

import com.blueharvest.account.model.Account;
import com.blueharvest.accountview.AccountTransactionView;
import com.blueharvest.accountview.repository.AccountRepository;
import com.blueharvest.accountview.repository.TransactionRepository;
import com.blueharvest.transaction.model.Transaction;
import java.util.List;

public class AccountTransactionViewImpl implements AccountTransactionView {

  private final AccountRepository accountRepository;
  private final TransactionRepository transactionRepository;

  public AccountTransactionViewImpl(
      AccountRepository accountRepository,
      TransactionRepository transactionRepository) {
    this.accountRepository = accountRepository;
    this.transactionRepository = transactionRepository;
  }

  @Override
  public void onAccountCreated(Account account) {
    accountRepository.save(account);
  }

  @Override
  public void onTransactionCreated(Transaction transaction) {
    transactionRepository.save(transaction);
  }

  @Override
  public List<Account> findByCustomerId(String customerId) {
    return accountRepository.findByCustomerId(customerId);
  }
}

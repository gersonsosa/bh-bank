package com.blueharvest.accountview.repository;

import com.blueharvest.transaction.model.Transaction;

public interface TransactionRepository {

  void save(Transaction transaction);

}

package com.blueharvest.accountview;

import com.blueharvest.account.model.Account;
import com.blueharvest.transaction.model.Transaction;
import java.util.List;

public interface AccountTransactionView {

  /**
   * Process account created event
   *
   * @param account account created
   */
  void onAccountCreated(Account account);

  /**
   * Process transaction created event
   *
   * @param transaction transaction created
   */
  void onTransactionCreated(Transaction transaction);

  /**
   * Retrieve accounts with transactions for the given customer id
   *
   * @param customerId customer unique identifier
   * @return a list of accounts with transactions
   */
  List<Account> findByCustomerId(String customerId);

}

package com.blueharvest.accountview.impl;

import com.blueharvest.account.model.Account;
import com.blueharvest.transaction.model.Transaction;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.UUID;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

class RandomModelGenerator {

  static Transaction createRandomTransaction() {
    final Transaction transaction = new Transaction();
    transaction.setId(UUID.randomUUID().toString());
    transaction.setAmount(new BigDecimal(RandomUtils.nextLong()));
    return transaction;
  }

  static Account createRandomAccount() {
    final Account account = new Account();
    account.setId(RandomStringUtils.randomAlphanumeric(12));
    account.setBalance(new BigDecimal(RandomUtils.nextLong()));
    account.setTransactions(Arrays.asList(createRandomTransaction(), createRandomTransaction()));
    account.setCustomerId(UUID.randomUUID().toString());
    return account;
  }

}

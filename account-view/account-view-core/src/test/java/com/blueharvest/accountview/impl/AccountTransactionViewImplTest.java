package com.blueharvest.accountview.impl;

import static com.blueharvest.accountview.impl.RandomModelGenerator.createRandomAccount;
import static com.blueharvest.accountview.impl.RandomModelGenerator.createRandomTransaction;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.blueharvest.account.model.Account;
import com.blueharvest.accountview.repository.AccountRepository;
import com.blueharvest.accountview.repository.TransactionRepository;
import com.blueharvest.transaction.model.Transaction;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class AccountTransactionViewImplTest {

  private AccountTransactionViewImpl subject;
  @Mock
  private AccountRepository accountRepository;
  @Mock
  private TransactionRepository transactionRepository;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new AccountTransactionViewImpl(accountRepository, transactionRepository);
  }

  @Test
  void shouldUpdateAccountCache() {
    final Account account = createRandomAccount();
    subject.onAccountCreated(account);

    verify(accountRepository).save(account);
  }

  @Test
  void shouldUpdateTransactionCache() {
    final Transaction transaction = createRandomTransaction();
    subject.onTransactionCreated(transaction);

    verify(transactionRepository).save(transaction);
  }

  @Test
  void shouldRetrieveAccountsWithTransactionsByCustomerId() {
    String customerId = UUID.randomUUID().toString();
    final List<Account> expected = Arrays.asList(
        createRandomAccount(), createRandomAccount(), createRandomAccount());
    when(accountRepository.findByCustomerId(customerId)).thenReturn(expected);

    final List<Account> accounts = subject.findByCustomerId(customerId);

    assertThat(accounts).satisfies(list -> {
      assertThat(list).isNotNull();
      assertThat(list).isEqualTo(expected);
    });
  }

}
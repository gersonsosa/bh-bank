package com.blueharvest.accountview.repository;

import com.blueharvest.account.model.Account;
import com.blueharvest.accountview.repository.jpa.model.AccountViewEntity;
import com.blueharvest.accountview.repository.jpa.model.TransactionViewEntity;
import com.blueharvest.transaction.model.Transaction;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

public class RandomModelGenerator {

  public static AccountViewEntity createRandomAccountEntityWith(String customerId) {
    final String accountId = UUID.randomUUID().toString();
    return new AccountViewEntity(accountId, new BigDecimal(RandomUtils.nextInt()), customerId,
        LocalDateTime.now(), Arrays
        .asList(createRandomTransactionEntityWith(accountId),
            createRandomTransactionEntityWith(accountId)));
  }

  static TransactionViewEntity createRandomTransactionEntityWith(String accountId) {
    return new TransactionViewEntity(UUID.randomUUID().toString(),
        new BigDecimal(RandomUtils.nextInt()), RandomStringUtils.randomAlphabetic(255),
        accountId, LocalDateTime.now());
  }

  public static Account createRandomAccount() {
    final String accountId = UUID.randomUUID().toString();
    return new Account(accountId, new BigDecimal(RandomUtils.nextInt()),
        UUID.randomUUID().toString(), LocalDateTime.now(), Collections.emptyList());
  }

  public static Transaction createRandomTransactionWith(String accounId) {
    return new Transaction(
        UUID.randomUUID().toString(),
        new BigDecimal(RandomUtils.nextFloat()),
        RandomStringUtils.randomAlphabetic(255), accounId, LocalDateTime.now());
  }
}
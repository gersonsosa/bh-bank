package com.blueharvest.accountview.repository.mapper;

import static com.blueharvest.accountview.repository.RandomModelGenerator.createRandomTransactionWith;

import com.blueharvest.accountview.repository.jpa.model.TransactionViewEntity;
import com.blueharvest.transaction.model.Transaction;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class TransactionMapperTest {

  @Test
  void shouldMapTransactionToTransactionViewEntity() {
    TransactionMapper subject = new TransactionMapper();
    final Transaction transaction = createRandomTransactionWith(
        UUID.randomUUID().toString());
    final TransactionViewEntity entity = subject.map(transaction);

    Assertions.assertThat(entity).isEqualToComparingFieldByField(transaction);
  }
}
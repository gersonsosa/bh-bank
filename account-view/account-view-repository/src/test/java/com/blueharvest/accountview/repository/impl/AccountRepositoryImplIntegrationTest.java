package com.blueharvest.accountview.repository.impl;

import static com.blueharvest.accountview.repository.RandomModelGenerator.createRandomAccount;
import static org.assertj.core.api.Assertions.assertThat;

import com.blueharvest.account.model.Account;
import com.blueharvest.accountview.repository.RandomModelGenerator;
import com.blueharvest.accountview.repository.TestRepositoryConfig;
import com.blueharvest.accountview.repository.jpa.AccountViewJpaRepository;
import com.blueharvest.accountview.repository.jpa.model.AccountViewEntity;
import com.blueharvest.accountview.repository.jpa.model.TransactionViewEntity;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@ContextConfiguration(classes = TestRepositoryConfig.class)
@Tag("integration")
class AccountRepositoryImplIntegrationTest {

  @Autowired
  private TestEntityManager testEntityManager;

  @Autowired
  private AccountViewJpaRepository accountViewJpaRepository;

  @Autowired
  private AccountRepositoryImpl subject;

  @Test
  void shouldSaveAccount() {
    Account account = createRandomAccount();
    subject.save(account);

    final Optional<AccountViewEntity> accountViewEntity = accountViewJpaRepository
        .findById(account.getId());
    assertThat(accountViewEntity).hasValueSatisfying(
        entity -> assertThat(entity).isEqualToIgnoringGivenFields(account, "transactions"));
  }

  @ParameterizedTest
  @MethodSource("accountListProvider")
  void shouldRetrieveAccountsWithTransactionsForCustomer(String customerId,
      List<AccountViewEntity> accountEntities, List<TransactionViewEntity> transactionEntities) {
    accountEntities.forEach(entity -> testEntityManager.persist(entity));
    transactionEntities.forEach(entity -> testEntityManager.persist(entity));

    final List<AccountViewEntity> accounts = accountViewJpaRepository
        .findByCustomerId(customerId);

    assertThat(accounts).usingRecursiveFieldByFieldElementComparator().isEqualTo(accountEntities);
  }

  private static Stream<Arguments> accountListProvider() {
    final String customerId = UUID.randomUUID().toString();
    final List<AccountViewEntity> accounts = Arrays.asList(
        RandomModelGenerator.createRandomAccountEntityWith(customerId),
        RandomModelGenerator.createRandomAccountEntityWith(customerId),
        RandomModelGenerator.createRandomAccountEntityWith(customerId));
    return Stream.of(
        Arguments.of(customerId, accounts,
            accounts.stream().map(AccountViewEntity::getTransactions)
                .flatMap(Collection::parallelStream).collect(Collectors.toList()))
    );
  }
}
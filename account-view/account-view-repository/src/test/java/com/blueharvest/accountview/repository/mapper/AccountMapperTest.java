package com.blueharvest.accountview.repository.mapper;

import static com.blueharvest.accountview.repository.RandomModelGenerator.createRandomAccount;
import static com.blueharvest.accountview.repository.RandomModelGenerator.createRandomAccountEntityWith;
import static org.assertj.core.api.Assertions.assertThat;

import com.blueharvest.account.model.Account;
import com.blueharvest.accountview.repository.jpa.model.AccountViewEntity;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AccountMapperTest {

  private AccountMapper subject;

  @BeforeEach
  void setUp() {
    subject = new AccountMapper();
  }

  @Test
  void shouldMapAccountToAccountEntity() {
    final Account source = createRandomAccount();
    final AccountViewEntity accountViewEntity = subject.map(source);

    assertThat(accountViewEntity).isEqualToIgnoringGivenFields(source, "transactions");
  }

  @Test
  void shouldMapAccountEntityToAccount() {
    final AccountViewEntity entity = createRandomAccountEntityWith(UUID.randomUUID().toString());
    final Account account = subject.map(entity);

    assertThat(account).isEqualToComparingFieldByFieldRecursively(entity);
  }

  @Test
  void shouldMapAccountEntityListToAccountList() {
    final AccountViewEntity source = createRandomAccountEntityWith(UUID.randomUUID().toString());
    final List<AccountViewEntity> list = Collections.singletonList(source);
    final List<Account> accounts = subject.map(list);

    assertThat(accounts).usingRecursiveFieldByFieldElementComparator().isEqualTo(list);
  }
}
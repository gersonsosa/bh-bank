package com.blueharvest.accountview.repository.impl;

import static org.assertj.core.api.Assertions.assertThat;

import com.blueharvest.accountview.repository.RandomModelGenerator;
import com.blueharvest.accountview.repository.TestRepositoryConfig;
import com.blueharvest.accountview.repository.jpa.TransactionViewJpaRepository;
import com.blueharvest.accountview.repository.jpa.model.AccountViewEntity;
import com.blueharvest.accountview.repository.jpa.model.TransactionViewEntity;
import com.blueharvest.transaction.model.Transaction;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@ContextConfiguration(classes = TestRepositoryConfig.class)
@Tag("integration")
class TransactionRepositoryImplIntegrationTest {

  @Autowired
  private TransactionRepositoryImpl subject;

  @Autowired
  private TransactionViewJpaRepository transactionViewJpaRepository;

  @Autowired
  private TestEntityManager testEntityManager;

  @ParameterizedTest
  @MethodSource("transactionProvider")
  void shouldSaveTransaction(AccountViewEntity accountViewEntity, Transaction transaction) {
    testEntityManager.persist(accountViewEntity);
    subject.save(transaction);

    final Optional<TransactionViewEntity> transactionViewEntity = transactionViewJpaRepository
        .findById(transaction.getId());

    assertThat(transactionViewEntity).hasValueSatisfying(
        entity -> assertThat(entity).isEqualToComparingFieldByField(transaction));
  }

  private static Stream<Arguments> transactionProvider() {
    final AccountViewEntity account = RandomModelGenerator
        .createRandomAccountEntityWith(UUID.randomUUID().toString());
    return Stream.of(
        Arguments.of(account, RandomModelGenerator.createRandomTransactionWith(account.getId()))
    );
  }
}
package com.blueharvest.accountview.repository;

import com.blueharvest.accountview.repository.impl.AccountRepositoryImpl;
import com.blueharvest.accountview.repository.jpa.AccountViewJpaRepository;
import com.blueharvest.accountview.repository.jpa.TransactionViewJpaRepository;
import com.blueharvest.accountview.repository.jpa.model.AccountViewEntity;
import com.blueharvest.accountview.repository.jpa.model.TransactionViewEntity;
import com.blueharvest.accountview.repository.mapper.AccountMapper;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = {AccountRepositoryImpl.class, AccountMapper.class})
public class TestRepositoryConfig {
}

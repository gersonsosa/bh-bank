package com.blueharvest.accountview.repository.jpa.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transaction_view")
public class TransactionViewEntity {

  @Id
  private String id;
  private BigDecimal amount;
  private String description;
  private String accountId;
  private LocalDateTime created;

  public TransactionViewEntity() {
  }

  public TransactionViewEntity(String id, BigDecimal amount, String description,
      String accountId, LocalDateTime created) {
    this.id = id;
    this.amount = amount;
    this.description = description;
    this.accountId = accountId;
    this.created = created;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getAccountId() {
    return accountId;
  }

  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  public LocalDateTime getCreated() {
    return created;
  }

  public void setCreated(LocalDateTime created) {
    this.created = created;
  }
}

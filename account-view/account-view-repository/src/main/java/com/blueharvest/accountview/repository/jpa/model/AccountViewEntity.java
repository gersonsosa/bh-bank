package com.blueharvest.accountview.repository.jpa.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "account_view")
public class AccountViewEntity {

  @Id
  private String id;
  private BigDecimal balance;
  private String customerId;
  private LocalDateTime created;
  @OneToMany
  @JoinColumn(name = "accountId")
  private List<TransactionViewEntity> transactions;

  public AccountViewEntity() {
  }

  public AccountViewEntity(String id, BigDecimal balance, String customerId,
      LocalDateTime created,
      List<TransactionViewEntity> transactions) {
    this.id = id;
    this.balance = balance;
    this.customerId = customerId;
    this.created = created;
    this.transactions = transactions;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public LocalDateTime getCreated() {
    return created;
  }

  public void setCreated(LocalDateTime created) {
    this.created = created;
  }

  public List<TransactionViewEntity> getTransactions() {
    return transactions;
  }

  public void setTransactions(List<TransactionViewEntity> transactions) {
    this.transactions = transactions;
  }
}

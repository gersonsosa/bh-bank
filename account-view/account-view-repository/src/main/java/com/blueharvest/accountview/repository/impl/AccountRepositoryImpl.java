package com.blueharvest.accountview.repository.impl;

import com.blueharvest.account.model.Account;
import com.blueharvest.accountview.repository.AccountRepository;
import com.blueharvest.accountview.repository.jpa.AccountViewJpaRepository;
import com.blueharvest.accountview.repository.mapper.AccountMapper;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class AccountRepositoryImpl implements AccountRepository {

  private final AccountViewJpaRepository accountViewJpaRepository;
  private final AccountMapper accountMapper;

  public AccountRepositoryImpl(
      AccountViewJpaRepository accountViewJpaRepository,
      AccountMapper accountMapper) {
    this.accountViewJpaRepository = accountViewJpaRepository;
    this.accountMapper = accountMapper;
  }

  @Override
  public void save(Account account) {
    accountViewJpaRepository.save(accountMapper.map(account));
  }

  @Override
  public List<Account> findByCustomerId(String customerId) {
    return accountMapper.map(accountViewJpaRepository.findByCustomerId(customerId));
  }
}

package com.blueharvest.accountview.repository.impl;

import com.blueharvest.accountview.repository.TransactionRepository;
import com.blueharvest.accountview.repository.jpa.TransactionViewJpaRepository;
import com.blueharvest.accountview.repository.mapper.TransactionMapper;
import com.blueharvest.transaction.model.Transaction;
import org.springframework.stereotype.Repository;

@Repository
public class TransactionRepositoryImpl implements TransactionRepository {

  private final TransactionViewJpaRepository transactionViewJpaRepository;
  private final TransactionMapper transactionMapper;

  public TransactionRepositoryImpl(
      TransactionViewJpaRepository transactionViewJpaRepository,
      TransactionMapper transactionMapper) {
    this.transactionViewJpaRepository = transactionViewJpaRepository;
    this.transactionMapper = transactionMapper;
  }

  @Override
  public void save(Transaction transaction) {
    transactionViewJpaRepository.save(transactionMapper.map(transaction));
  }
}

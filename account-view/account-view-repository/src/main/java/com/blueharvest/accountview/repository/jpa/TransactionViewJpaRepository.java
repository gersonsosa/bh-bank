package com.blueharvest.accountview.repository.jpa;

import com.blueharvest.accountview.repository.jpa.model.TransactionViewEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionViewJpaRepository extends JpaRepository<TransactionViewEntity, String> {
}

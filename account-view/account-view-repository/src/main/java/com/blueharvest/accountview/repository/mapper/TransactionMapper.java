package com.blueharvest.accountview.repository.mapper;

import com.blueharvest.accountview.repository.jpa.model.TransactionViewEntity;
import com.blueharvest.transaction.model.Transaction;
import com.googlecode.jmapper.JMapper;
import com.googlecode.jmapper.api.Global;
import com.googlecode.jmapper.api.JMapperAPI;
import org.springframework.stereotype.Component;

@Component
public class TransactionMapper {

  private final JMapper<TransactionViewEntity, Transaction> transactionViewEntityMapper;

  TransactionMapper() {
    JMapperAPI jMapperAPI = new JMapperAPI()
        .add(JMapperAPI.mappedClass(TransactionViewEntity.class).add(new Global()))
        .add(JMapperAPI.mappedClass(TransactionViewEntity.class).add(new Global()));
    transactionViewEntityMapper = new JMapper<>(TransactionViewEntity.class, Transaction.class,
        jMapperAPI);
  }

  public TransactionViewEntity map(Transaction transaction) {
    return transactionViewEntityMapper.getDestination(transaction);
  }

}

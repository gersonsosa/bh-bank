package com.blueharvest.accountview.repository.jpa;

import com.blueharvest.accountview.repository.jpa.model.AccountViewEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountViewJpaRepository extends JpaRepository<AccountViewEntity, String> {

  List<AccountViewEntity> findByCustomerId(String customerId);
}

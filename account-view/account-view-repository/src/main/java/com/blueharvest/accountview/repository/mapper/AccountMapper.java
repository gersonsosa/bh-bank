package com.blueharvest.accountview.repository.mapper;

import static com.googlecode.jmapper.api.JMapperAPI.mappedClass;

import com.blueharvest.account.model.Account;
import com.blueharvest.accountview.repository.jpa.model.AccountViewEntity;
import com.blueharvest.accountview.repository.jpa.model.TransactionViewEntity;
import com.blueharvest.transaction.model.Transaction;
import com.googlecode.jmapper.JMapper;
import com.googlecode.jmapper.api.Global;
import com.googlecode.jmapper.api.JMapperAPI;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class AccountMapper {

  private final JMapper<AccountViewEntity, Account> accountViewEntityJMapper;
  private final JMapper<Account, AccountViewEntity> accountJMapper;

  AccountMapper() {
    final JMapperAPI jMapperAPI = new JMapperAPI()
        .add(mappedClass(AccountViewEntity.class)
            .add(new Global().excludedAttributes("transactions")))
        .add(mappedClass(TransactionViewEntity.class).add(new Global()))
        .add(mappedClass(Account.class).add(new Global()))
        .add(mappedClass(Transaction.class).add(new Global()));
    accountViewEntityJMapper = new JMapper<>(AccountViewEntity.class, Account.class, jMapperAPI);
    accountJMapper = new JMapper<>(Account.class, AccountViewEntity.class, jMapperAPI);
  }

  public AccountViewEntity map(Account account) {
    return accountViewEntityJMapper.getDestination(account);
  }

  Account map(AccountViewEntity entity) {
    return accountJMapper.getDestination(entity);
  }

  public List<Account> map(List<AccountViewEntity> entities) {
    return entities.parallelStream().map(this::map).collect(Collectors.toList());
  }

}

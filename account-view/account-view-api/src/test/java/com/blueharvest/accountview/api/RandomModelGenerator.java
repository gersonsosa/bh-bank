package com.blueharvest.accountview.api;

import com.blueharvest.account.model.Account;
import com.blueharvest.transaction.model.Transaction;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.UUID;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

final class RandomModelGenerator {

  static Account createRandomAccount() {
    return new Account(UUID.randomUUID().toString(),
        new BigDecimal(RandomUtils.nextLong()),
        UUID.randomUUID().toString(),
        LocalDateTime.now(),
        Arrays.asList(createRandomTransaction(), createRandomTransaction(),
            createRandomTransaction()));
  }

  static Account createRandomAccountWith(String customerId) {
    return new Account(UUID.randomUUID().toString(),
        new BigDecimal(RandomUtils.nextLong()),
        customerId,
        LocalDateTime.now(),
        Arrays.asList(createRandomTransaction(), createRandomTransaction(),
            createRandomTransaction()));
  }

  static Transaction createRandomTransaction() {
    return new Transaction(UUID.randomUUID().toString(), new BigDecimal(RandomUtils.nextLong()),
        RandomStringUtils.randomAlphabetic(255), UUID.randomUUID().toString(), LocalDateTime.now());
  }

}

package com.blueharvest.accountview.api;

import static com.blueharvest.accountview.api.RandomModelGenerator.createRandomAccount;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.blueharvest.account.model.Account;
import com.blueharvest.accountview.AccountTransactionView;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class AccountViewControllerTest {

  private AccountViewController subject;
  @Mock
  private AccountTransactionView accountTransactionView;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new AccountViewController(accountTransactionView);
  }

  @Test
  void shouldReturnTheListOfAccountsByCustomerId() {
    String customerId = UUID.randomUUID().toString();
    List<Account> expected = Arrays.asList(createRandomAccount(), createRandomAccount());
    when(accountTransactionView.findByCustomerId(customerId)).thenReturn(expected);

    final List<Account> accounts = subject.findByCustomerId(customerId);

    assertThat(accounts).isEqualTo(expected);
  }
}
package com.blueharvest.accountview.api;

import static com.blueharvest.accountview.api.RandomModelGenerator.createRandomAccountWith;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.blueharvest.account.model.Account;
import com.blueharvest.accountview.AccountTransactionView;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@Tag("integration")
class AccountViewControllerIntegrationTest {

  @Autowired
  private MockMvc mvc;

  @Autowired
  private ObjectMapper objectMapper;

  @MockBean
  private AccountTransactionView accountTransactionView;

  @Test
  void shouldRetrieveAccountListWithTransactionsFor() throws Exception {
    final String customerId = UUID.randomUUID().toString();
    List<Account> accountEntities = Arrays
        .asList(createRandomAccountWith(customerId), createRandomAccountWith(customerId),
            createRandomAccountWith(customerId));

    given(accountTransactionView.findByCustomerId(customerId))
        .willReturn(accountEntities);

    mvc.perform(get("/accounts-view").param("customerId", customerId)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.length()", is(accountEntities.size())))
        .andExpect(jsonPath("$[0].customerId", is(customerId)));
  }

}
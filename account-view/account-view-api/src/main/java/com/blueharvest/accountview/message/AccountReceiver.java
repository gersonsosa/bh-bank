package com.blueharvest.accountview.message;

import com.blueharvest.account.model.Account;
import com.blueharvest.accountview.AccountTransactionView;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class AccountReceiver {

  private final AccountTransactionView accountTransactionView;

  public AccountReceiver(AccountTransactionView accountTransactionView) {
    this.accountTransactionView = accountTransactionView;
  }

  @RabbitListener(queues = "accountsCreated")
  public void receiveAccount(Account account) {
    accountTransactionView.onAccountCreated(account);
  }
}

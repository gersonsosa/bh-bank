package com.blueharvest.accountview.api;

import com.blueharvest.account.model.Account;
import com.blueharvest.accountview.AccountTransactionView;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("accounts-view")
class AccountViewController {

  private final AccountTransactionView accountTransactionView;

  AccountViewController(
      AccountTransactionView accountTransactionView) {
    this.accountTransactionView = accountTransactionView;
  }

  @GetMapping
  List<Account> findByCustomerId(@RequestParam String customerId) {
    return accountTransactionView.findByCustomerId(customerId);
  }

}

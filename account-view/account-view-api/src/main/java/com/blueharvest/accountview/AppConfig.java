package com.blueharvest.accountview;

import com.blueharvest.accountview.impl.AccountTransactionViewImpl;
import com.blueharvest.accountview.repository.AccountRepository;
import com.blueharvest.accountview.repository.TransactionRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SuppressWarnings("unused")
@Configuration
public class AppConfig {

  @Bean
  public AccountTransactionView accountTransactionView(
      AccountRepository accountRepository,
      TransactionRepository transactionRepository) {
    return new AccountTransactionViewImpl(accountRepository, transactionRepository);
  }
}

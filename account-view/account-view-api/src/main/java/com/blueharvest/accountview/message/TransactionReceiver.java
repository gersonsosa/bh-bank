package com.blueharvest.accountview.message;

import com.blueharvest.accountview.AccountTransactionView;
import com.blueharvest.transaction.model.Transaction;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class TransactionReceiver {

  private final AccountTransactionView accountTransactionView;

  public TransactionReceiver(AccountTransactionView accountTransactionView) {
    this.accountTransactionView = accountTransactionView;
  }

  @RabbitListener(queues = "transactionsCreated")
  public void receiveTransaction(Transaction transaction) {
    accountTransactionView.onTransactionCreated(transaction);
  }

}

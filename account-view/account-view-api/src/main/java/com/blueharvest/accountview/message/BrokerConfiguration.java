package com.blueharvest.accountview.message;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BrokerConfiguration {

  @Bean
  public Exchange accountCreatedExchange() {
    return new TopicExchange("accountCreatedExchange");
  }

  @Bean
  public Queue accountQueue() {
    return new Queue("accountsCreated");
  }

  @Bean
  public Binding binding(@Qualifier("accountQueue") Queue queue,
      @Qualifier("accountCreatedExchange") Exchange eventExchange) {
    return BindingBuilder
        .bind(queue)
        .to(eventExchange)
        .with("account.*")
        .noargs();
  }

  @Bean
  public Exchange transactionCreatedExchange() {
    return new TopicExchange("transactionCreatedExchange");
  }

  @Bean
  public Queue transactionCreatedQueue() {
    return new Queue("transactionsCreated");
  }

  @Bean
  public Binding transactionBinding(@Qualifier("transactionCreatedQueue") Queue queue,
      @Qualifier("transactionCreatedExchange") Exchange eventExchange) {
    return BindingBuilder
        .bind(queue)
        .to(eventExchange)
        .with("transaction.created")
        .noargs();
  }

}

package com.blueharvest.accountview.api.client;

import com.blueharvest.account.model.Account;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitClient {

  @GET("accounts-view")
  Call<List<Account>> findByCustomerId(@Query("customerId") String customerId);

}

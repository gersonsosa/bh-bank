package com.blueharvest.accountview.api.client;

import com.blueharvest.account.model.Account;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Component
public class AccountViewApiClient {

  private final RetrofitClient retrofitClient;

  AccountViewApiClient(@Value("${accounts.transactions.api.url}") String baseUrl) {
    final Gson gson = new GsonBuilder()
        .registerTypeAdapter(LocalDateTime.class,
            (JsonDeserializer<LocalDateTime>) (json, type, context) -> LocalDateTime
                .parse(json.getAsJsonPrimitive().getAsString())).create();

    final Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(
        GsonConverterFactory.create(gson)).build();
    this.retrofitClient = retrofit.create(RetrofitClient.class);
  }

  public List<Account> findByCustomerId(String customerId) throws AccountViewClientException {
    try {
      final Response<List<Account>> response = retrofitClient
          .findByCustomerId(customerId)
          .execute();
      if (response.isSuccessful()) {
        return response.body();
      } else {
        throw new AccountViewClientException(response.code(), Objects
            .requireNonNull(response.errorBody()).string());
      }
    } catch (IOException e) {
      throw new AccountViewClientException(e);
    }
  }
}

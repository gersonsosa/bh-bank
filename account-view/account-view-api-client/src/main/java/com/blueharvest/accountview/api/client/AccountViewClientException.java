package com.blueharvest.accountview.api.client;

public class AccountViewClientException extends Exception {

  private static final String CLIENT_ERROR_MESSAGE = "An error occurred invoking the account-view-api with response code: %d and message %s";

  public AccountViewClientException(Exception parent) {
    super(parent);
  }

  public AccountViewClientException(int code, String menssage) {
    super(String.format(CLIENT_ERROR_MESSAGE, code, menssage));
  }
}

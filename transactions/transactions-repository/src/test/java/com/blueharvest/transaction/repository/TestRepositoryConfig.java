package com.blueharvest.transaction.repository;

import com.blueharvest.transaction.repository.impl.TransactionRepositoryImpl;
import com.blueharvest.transaction.repository.mapper.TransactionMapper;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = {TransactionRepositoryImpl.class, TransactionMapper.class})
public class TestRepositoryConfig {
}

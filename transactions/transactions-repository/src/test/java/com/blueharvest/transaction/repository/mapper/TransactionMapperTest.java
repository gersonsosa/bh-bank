package com.blueharvest.transaction.repository.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import com.blueharvest.transaction.model.Transaction;
import com.blueharvest.transaction.repository.jpa.model.TransactionEntity;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class TransactionMapperTest {

  private TransactionMapper subject;

  @BeforeEach
  void setUp() {
    subject = new TransactionMapper();
  }

  @ParameterizedTest
  @MethodSource("transactionMapperProvider")
  void shouldMapTransactionEntityToTransaction(String transactionId, String accountId,
      Long amount, String description) {
    final TransactionEntity entity = new TransactionEntity();
    entity.setId(transactionId);
    entity.setAccountId(accountId);
    entity.setAmount(new BigDecimal(amount));
    entity.setDescription(description);
    Transaction transaction = subject.map(entity);

    assertThat(transaction).isEqualToComparingFieldByField(entity);
  }

  @ParameterizedTest
  @MethodSource("transactionMapperProvider")
  void shouldMapTransactionToTransactionEntity(String transactionId, String accountId,
      Long amount, String description) {
    final Transaction transaction = new Transaction();
    transaction.setId(transactionId);
    transaction.setAccountId(accountId);
    transaction.setAmount(new BigDecimal(amount));
    transaction.setDescription(description);
    TransactionEntity transactionEntity = subject.map(transaction);

    assertThat(transactionEntity).isEqualToComparingFieldByField(transaction);
  }

  @ParameterizedTest
  @MethodSource("transactionMapperProvider")
  void shouldMapTransactionEntityListToTransactionList(String transactionId, String accountId,
      Long amount, String description) {
    final TransactionEntity transaction = new TransactionEntity();
    transaction.setId(transactionId);
    transaction.setAccountId(accountId);
    transaction.setAmount(new BigDecimal(amount));
    transaction.setDescription(description);
    final List<TransactionEntity> entities = Collections.singletonList(transaction);
    List<Transaction> transactions = subject.map(entities);

    assertThat(transactions).usingFieldByFieldElementComparator().isEqualTo(entities);
  }

  private static Stream<Arguments> transactionMapperProvider() {
    final String s = RandomStringUtils.randomAlphabetic(12);
    return Stream.of(
        Arguments.of(null, null, 0L, null, null),
        Arguments.of("", "", 0L, ""),
        Arguments.of(" ", " ", 1L, " "),
        Arguments.of(s, s, 1L, s),
        Arguments.of(s, s, -1L, s)
    );
  }

}
package com.blueharvest.transaction.repository.impl;

import static com.blueharvest.transaction.repository.RandomModelGenerator.createRandomTransactionWithoutId;
import static org.assertj.core.api.Assertions.assertThat;

import com.blueharvest.transaction.model.Transaction;
import com.blueharvest.transaction.repository.TestRepositoryConfig;
import com.blueharvest.transaction.repository.jpa.TransactionJpaRepository;
import com.blueharvest.transaction.repository.jpa.model.TransactionEntity;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@ContextConfiguration(classes = TestRepositoryConfig.class)
@Tag("integration")
class TransactionRepositoryImplIntegrationTest {

  @Autowired
  private TransactionRepositoryImpl subject;

  @Autowired
  private TestEntityManager testEntityManager;

  @Autowired
  private TransactionJpaRepository transactionJpaRepository;

  @Test
  void shouldSaveTransactionInDatabase() {
    final Transaction source = createRandomTransactionWithoutId();
    final Transaction transaction = subject.save(source);

    final Optional<TransactionEntity> optionalTransactionEntity = transactionJpaRepository
        .findById(transaction.getId());

    assertThat(optionalTransactionEntity).hasValueSatisfying(
        entity -> assertThat(entity).isEqualToComparingFieldByField(transaction));
  }

  @Test
  void shouldRetrieveTransactionByAccountId() {
    final String accountId = UUID.randomUUID().toString();
    List<TransactionEntity> transactions = Arrays
        .asList(createRandomTransactionEntity(accountId), createRandomTransactionEntity(accountId));
    transactions.forEach(entity -> testEntityManager.persist(entity));

    final List<Transaction> resultList = subject.findByAccountId(accountId);

    assertThat(resultList).usingRecursiveFieldByFieldElementComparator().isEqualTo(transactions);
  }

  private TransactionEntity createRandomTransactionEntity(String accountId) {
    return new TransactionEntity(null, new BigDecimal(
        RandomUtils.nextInt()), RandomStringUtils.randomAlphabetic(255), accountId,
        LocalDateTime.now());
  }
}
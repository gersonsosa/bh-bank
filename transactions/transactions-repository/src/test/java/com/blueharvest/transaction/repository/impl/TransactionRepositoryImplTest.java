package com.blueharvest.transaction.repository.impl;

import static com.blueharvest.transaction.repository.RandomModelGenerator.createRandomTransaction;
import static com.blueharvest.transaction.repository.RandomModelGenerator.createRandomTransactionEntity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.blueharvest.transaction.model.Transaction;
import com.blueharvest.transaction.repository.TransactionRepository;
import com.blueharvest.transaction.repository.jpa.TransactionJpaRepository;
import com.blueharvest.transaction.repository.jpa.model.TransactionEntity;
import com.blueharvest.transaction.repository.mapper.TransactionMapper;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class TransactionRepositoryImplTest {

  @Mock
  private TransactionJpaRepository transactionJpaRepository;
  @Mock
  private TransactionMapper transactionMapper;
  private TransactionRepository subject;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    this.subject = new TransactionRepositoryImpl(transactionJpaRepository, transactionMapper);
  }

  @Test
  void shouldSaveTransactionAndReturnIt() {
    final Transaction expected = createRandomTransaction();
    final TransactionEntity entity = createRandomTransactionEntity();

    when(transactionMapper.map(expected)).thenReturn(entity);
    when(transactionJpaRepository.save(entity)).thenReturn(entity);
    when(transactionMapper.map(entity)).thenReturn(expected);

    Transaction result = subject.save(expected);

    assertThat(result).isEqualToComparingFieldByField(expected);
  }

  @Test
  void shouldRetrieveTransactionsByAccountId() {
    String accountId = RandomStringUtils.randomAlphanumeric(12);
    List<TransactionEntity> entities = Arrays
        .asList(createRandomTransactionEntity(), createRandomTransactionEntity());

    List<Transaction> expected = Arrays
        .asList(createRandomTransaction(), createRandomTransaction());
    when(transactionJpaRepository.findByAccountId(accountId)).thenReturn(entities);
    when(transactionMapper.map(entities)).thenReturn(expected);

    final List<Transaction> transactions = subject.findByAccountId(accountId);
    assertThat(transactions).isEqualTo(expected);
  }
}
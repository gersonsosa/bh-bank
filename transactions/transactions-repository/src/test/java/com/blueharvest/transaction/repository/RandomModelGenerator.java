package com.blueharvest.transaction.repository;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import com.blueharvest.transaction.model.Transaction;
import com.blueharvest.transaction.repository.jpa.model.TransactionEntity;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;
import org.apache.commons.lang3.RandomUtils;

public class RandomModelGenerator {

  public static Transaction createRandomTransactionWithoutId() {
    return new Transaction(null, new BigDecimal(RandomUtils.nextFloat()),
        randomAlphabetic(255), UUID.randomUUID().toString(), LocalDateTime.now());
  }

  public static Transaction createRandomTransaction() {
    final Transaction transaction = createRandomTransactionWithoutId();
    transaction.setId(UUID.randomUUID().toString());
    return transaction;
  }

  public static TransactionEntity createRandomTransactionEntity() {
    return new TransactionEntity(UUID.randomUUID().toString(),
        new BigDecimal(RandomUtils.nextFloat()), randomAlphabetic(255),
        UUID.randomUUID().toString(), LocalDateTime.now());
  }

}

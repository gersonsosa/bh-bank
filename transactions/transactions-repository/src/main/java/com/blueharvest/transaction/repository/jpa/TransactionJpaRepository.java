package com.blueharvest.transaction.repository.jpa;

import com.blueharvest.transaction.repository.jpa.model.TransactionEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionJpaRepository extends JpaRepository<TransactionEntity, String> {

  List<TransactionEntity> findByAccountId(String accountId);
}

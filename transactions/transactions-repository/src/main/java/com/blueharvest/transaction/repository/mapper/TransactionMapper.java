package com.blueharvest.transaction.repository.mapper;

import com.blueharvest.transaction.model.Transaction;
import com.blueharvest.transaction.repository.jpa.model.TransactionEntity;
import com.googlecode.jmapper.JMapper;
import com.googlecode.jmapper.api.Global;
import com.googlecode.jmapper.api.JMapperAPI;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class TransactionMapper {

  private JMapper<Transaction, TransactionEntity> transactionEntityJMapper;
  private JMapper<TransactionEntity, Transaction> transactionJMapper;

  public TransactionMapper() {
    JMapperAPI jMapperAPI = new JMapperAPI()
        .add(JMapperAPI.mappedClass(Transaction.class).add(new Global()))
        .add(JMapperAPI.mappedClass(TransactionEntity.class).add(new Global()));
    transactionEntityJMapper = new JMapper<>(Transaction.class, TransactionEntity.class,
        jMapperAPI);
    transactionJMapper = new JMapper<>(TransactionEntity.class, Transaction.class, jMapperAPI);
  }

  public Transaction map(TransactionEntity entity) {
    return transactionEntityJMapper.getDestination(entity);
  }

  public TransactionEntity map(Transaction transaction) {
    return transactionJMapper.getDestination(transaction);
  }

  public List<Transaction> map(List<TransactionEntity> transactionEntities) {
    return transactionEntities.parallelStream().map(this::map).collect(Collectors.toList());
  }
}

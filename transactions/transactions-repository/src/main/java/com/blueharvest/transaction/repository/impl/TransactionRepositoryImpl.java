package com.blueharvest.transaction.repository.impl;

import com.blueharvest.transaction.model.Transaction;
import com.blueharvest.transaction.repository.TransactionRepository;
import com.blueharvest.transaction.repository.jpa.TransactionJpaRepository;
import com.blueharvest.transaction.repository.mapper.TransactionMapper;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class TransactionRepositoryImpl implements TransactionRepository {

  private final TransactionJpaRepository transactionJpaRepository;
  private final TransactionMapper transactionMapper;

  TransactionRepositoryImpl(
      TransactionJpaRepository transactionJpaRepository,
      TransactionMapper transactionMapper) {
    this.transactionJpaRepository = transactionJpaRepository;
    this.transactionMapper = transactionMapper;
  }

  @Override
  public Transaction save(Transaction transaction) {
    return transactionMapper.map(transactionJpaRepository.save(transactionMapper.map(transaction)));
  }

  @Override
  public List<Transaction> findByAccountId(String accountId) {
    return transactionMapper.map(transactionJpaRepository.findByAccountId(accountId));
  }
}

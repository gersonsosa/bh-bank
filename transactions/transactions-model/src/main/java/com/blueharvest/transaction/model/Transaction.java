package com.blueharvest.transaction.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Transaction implements Serializable {

  private String id;
  private BigDecimal amount;
  private String description;
  private String accountId;
  private LocalDateTime created;

  public Transaction() {
  }

  public Transaction(String id, BigDecimal amount, String description, String accountId,
      LocalDateTime created) {
    this.id = id;
    this.amount = amount;
    this.description = description;
    this.accountId = accountId;
    this.created = created;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getAccountId() {
    return accountId;
  }

  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  public LocalDateTime getCreated() {
    return created;
  }

  public void setCreated(LocalDateTime created) {
    this.created = created;
  }
}

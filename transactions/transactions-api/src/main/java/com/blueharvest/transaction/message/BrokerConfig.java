package com.blueharvest.transaction.message;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BrokerConfig {

  @Bean
  public Exchange transactionExchange() {
    return new TopicExchange("transactionExchange");
  }

  @Bean
  public Exchange transactionCreatedExchange() {
    return new TopicExchange("transactionCreatedExchange");
  }

  @Bean
  public Queue transactionQueue() {
    return new Queue("transactions");
  }

  @Bean
  public Binding transactionBinding(Queue queue, Exchange transactionExchange) {
    return BindingBuilder
        .bind(queue)
        .to(transactionExchange)
        .with("transaction.create")
        .noargs();
  }

}

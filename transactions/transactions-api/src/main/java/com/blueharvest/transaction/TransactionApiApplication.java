package com.blueharvest.transaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@ComponentScan(
    basePackages = {
        "com.blueharvest.account.api.client",
        "com.blueharvest.transaction"
    }
)
@EnableSwagger2
@SpringBootApplication
public class TransactionApiApplication {

  public static void main(String[] args) {
    SpringApplication.run(TransactionApiApplication.class, args);
  }

}

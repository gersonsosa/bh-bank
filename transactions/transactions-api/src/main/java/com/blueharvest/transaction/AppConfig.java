package com.blueharvest.transaction;

import com.blueharvest.transaction.repository.AccountRepository;
import com.blueharvest.transaction.repository.TransactionRepository;
import com.blueharvest.transaction.service.TransactionMessageSender;
import com.blueharvest.transaction.service.TransactionService;
import com.blueharvest.transaction.service.impl.TransactionServiceImpl;
import java.time.Clock;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class AppConfig {

  @Bean
  public Clock clock() {
    return Clock.systemDefaultZone();
  }

  @Bean
  public TransactionService transactionService(
      Clock clock,
      AccountRepository accountRepository,
      TransactionRepository transactionRepository,
      TransactionMessageSender transactionMessageSender) {
    return new TransactionServiceImpl(clock, accountRepository, transactionRepository,
        transactionMessageSender);
  }

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.regex("/transactions.*"))
        .build()
        .apiInfo(apiInfo());
  }

  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title("Transactions API")
        .description("Api to handle all Transactions operations")
        .build();
  }

}

package com.blueharvest.transaction.external.account;

import com.blueharvest.account.api.client.AccountsApiClient;
import com.blueharvest.account.api.client.AccountsClientException;
import com.blueharvest.transaction.repository.AccountRepository;
import com.blueharvest.transaction.repository.TransactionCreationException;
import org.springframework.stereotype.Repository;

@Repository
public class AccountApi implements AccountRepository {

  private final AccountsApiClient accountsApiClient;

  AccountApi(AccountsApiClient accountsApiClient) {
    this.accountsApiClient = accountsApiClient;
  }

  @Override
  public boolean exists(String accountId) throws TransactionCreationException {
    try {
      return accountsApiClient.exists(accountId);
    } catch (AccountsClientException e) {
      throw new TransactionCreationException(e);
    }
  }
}

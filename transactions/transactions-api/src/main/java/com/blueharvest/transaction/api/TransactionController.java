package com.blueharvest.transaction.api;

import com.blueharvest.transaction.api.model.TransactionRequest;
import com.blueharvest.transaction.model.Transaction;
import com.blueharvest.transaction.repository.TransactionCreationException;
import com.blueharvest.transaction.service.TransactionService;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("transactions")
class TransactionController {

  private final TransactionService transactionService;

  TransactionController(TransactionService transactionService) {
    this.transactionService = transactionService;
  }

  @PostMapping
  ResponseEntity<Transaction> create(@RequestBody TransactionRequest transactionRequest)
      throws TransactionCreationException {
    Transaction transaction = transactionService.create(
        transactionRequest.getAccountId(), new BigDecimal(transactionRequest.getAmount()),
        transactionRequest.getDescription());
    return ResponseEntity.ok(transaction);
  }

  @GetMapping
  ResponseEntity<List<Transaction>> findByAccountId(
      @RequestParam(value = "accountId") String accountId) {
    return ResponseEntity.ok(transactionService.findByAccountId(accountId));
  }

}

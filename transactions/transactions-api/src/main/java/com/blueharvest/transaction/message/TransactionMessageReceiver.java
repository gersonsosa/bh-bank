package com.blueharvest.transaction.message;

import com.blueharvest.transaction.api.model.TransactionRequest;
import com.blueharvest.transaction.repository.TransactionCreationException;
import com.blueharvest.transaction.service.TransactionService;
import java.math.BigDecimal;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
class TransactionMessageReceiver {

  private final TransactionService transactionService;

  TransactionMessageReceiver(TransactionService transactionService) {
    this.transactionService = transactionService;
  }

  @RabbitListener(queues = "transactions")
  void receiveTransaction(TransactionRequest transactionRequest)
      throws TransactionCreationException {
    transactionService.create(transactionRequest.getAccountId(),
        new BigDecimal(transactionRequest.getAmount()),
        transactionRequest.getDescription());
  }
}

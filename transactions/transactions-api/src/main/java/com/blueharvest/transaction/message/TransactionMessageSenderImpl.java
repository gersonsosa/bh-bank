package com.blueharvest.transaction.message;

import com.blueharvest.transaction.model.Transaction;
import com.blueharvest.transaction.service.TransactionMessageSender;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class TransactionMessageSenderImpl implements TransactionMessageSender {

  private final RabbitTemplate rabbitTemplate;
  private final Exchange transactionCreatedExchange;

  TransactionMessageSenderImpl(
      RabbitTemplate rabbitTemplate, Exchange transactionCreatedExchange) {
    this.rabbitTemplate = rabbitTemplate;
    this.transactionCreatedExchange = transactionCreatedExchange;
  }

  @Override
  public void onTransactionCreated(Transaction transaction) {
    rabbitTemplate
        .convertAndSend(transactionCreatedExchange.getName(), "transaction.created", transaction);
  }
}

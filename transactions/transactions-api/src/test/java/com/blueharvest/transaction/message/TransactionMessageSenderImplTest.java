package com.blueharvest.transaction.message;

import static com.blueharvest.transaction.RandomModelGenerator.createRandomTransaction;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.blueharvest.transaction.model.Transaction;
import com.blueharvest.transaction.service.TransactionMessageSender;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

class TransactionMessageSenderImplTest {

  private TransactionMessageSender subject;
  @Mock
  private RabbitTemplate rabbitTemplate;
  @Mock
  private Exchange exchange;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new TransactionMessageSenderImpl(rabbitTemplate, exchange);
  }

  @Test
  void shouldSendMessageToBrokerWhenTransactionIsCreated() {
    final String exchangeName = "transactionExchange";
    when(exchange.getName()).thenReturn(exchangeName);
    final Transaction transaction = createRandomTransaction(UUID.randomUUID().toString());
    subject.onTransactionCreated(transaction);

    verify(rabbitTemplate).convertAndSend(exchangeName, "transaction.created", transaction);
  }
}
package com.blueharvest.transaction.message;

import static org.mockito.Mockito.verify;

import com.blueharvest.transaction.api.model.TransactionRequest;
import com.blueharvest.transaction.repository.TransactionCreationException;
import com.blueharvest.transaction.service.TransactionService;
import java.math.BigDecimal;
import java.util.UUID;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class TransactionMessageReceiverTest {

  private TransactionMessageReceiver subject;
  @Mock
  private TransactionService transactionService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new TransactionMessageReceiver(transactionService);
  }

  @Test
  void shouldCreateAccountWhenRequestMessageArrives() throws TransactionCreationException {
    final String accountId = UUID.randomUUID().toString();
    final String amount = "0";
    final String description = RandomStringUtils.randomAlphabetic(255);
    subject.receiveTransaction(new TransactionRequest(accountId, amount, description));

    verify(transactionService).create(accountId, new BigDecimal(amount), description);
  }
}
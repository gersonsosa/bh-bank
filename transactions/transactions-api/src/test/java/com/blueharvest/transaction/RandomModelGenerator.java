package com.blueharvest.transaction;

import com.blueharvest.transaction.model.Transaction;
import org.apache.commons.lang3.RandomStringUtils;

public class RandomModelGenerator {

  public static Transaction createRandomTransaction(String accountId) {
    final Transaction transaction = new Transaction();
    transaction.setId(RandomStringUtils.randomAlphanumeric(12));
    transaction.setAccountId(accountId);
    return transaction;
  }

}

package com.blueharvest.transaction.external.account;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.when;

import com.blueharvest.account.api.client.AccountsApiClient;
import com.blueharvest.account.api.client.AccountsClientException;
import com.blueharvest.transaction.repository.AccountRepository;
import com.blueharvest.transaction.repository.TransactionCreationException;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class AccountApiTest {

  private AccountRepository subject;
  @Mock
  private AccountsApiClient accountsApiClient;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new AccountApi(accountsApiClient);
  }

  @Test
  void shouldReturnTrueWhenAccountExists() throws Exception {
    String accountId = UUID.randomUUID().toString();
    when(accountsApiClient.exists(accountId)).thenReturn(true);
    assertThat(subject.exists(accountId)).isTrue();
  }

  @Test
  void shouldThrowTransactionCreationExceptionIfAccountApiCallFails()
      throws AccountsClientException {
    String accountId = UUID.randomUUID().toString();
    when(accountsApiClient.exists(accountId))
        .thenThrow(new AccountsClientException(500, "Sample Exception Description"));
    assertThatExceptionOfType(TransactionCreationException.class)
        .isThrownBy(() -> subject.exists(accountId));
  }
}
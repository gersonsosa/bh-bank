package com.blueharvest.transaction.api;

import static com.blueharvest.transaction.RandomModelGenerator.createRandomTransaction;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.blueharvest.transaction.api.model.TransactionRequest;
import com.blueharvest.transaction.model.Transaction;
import com.blueharvest.transaction.repository.TransactionCreationException;
import com.blueharvest.transaction.service.TransactionService;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

class TransactionControllerTest {

  @Mock
  private TransactionService transactionService;

  private TransactionController subject;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    this.subject = new TransactionController(transactionService);
  }

  @ParameterizedTest
  @MethodSource("apiRequestProvider")
  void shouldCreateTransactionAndReturn(String transactionId, String accountId, BigDecimal amount,
      String description) throws TransactionCreationException {
    final Transaction transaction = new Transaction(transactionId, amount, description, accountId,
        LocalDateTime.now());
    when(transactionService.create(accountId, amount, description)).thenReturn(transaction);

    ResponseEntity<Transaction> responseEntity = subject
        .create(new TransactionRequest(accountId, amount.toString(), description));

    assertThat(responseEntity).satisfies(
        response -> {
          assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
          assertThat(response.getBody()).isEqualToComparingFieldByField(transaction);
        }
    );
  }

  @Test
  void shouldRetrieveTransactionsByAccountId() {
    final String accountId = UUID.randomUUID().toString();

    final List<Transaction> expected = Arrays.asList(
        createRandomTransaction(accountId), createRandomTransaction(accountId));
    when(transactionService.findByAccountId(accountId)).thenReturn(expected);
    final ResponseEntity<List<Transaction>> responseEntity = subject.findByAccountId(accountId);

    assertThat(responseEntity).satisfies(
        response -> {
          assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
          assertThat(response.getBody()).isEqualTo(expected);
        }
    );
  }

  private static Stream<Arguments> apiRequestProvider() {
    final String description = "Test transaction";
    return Stream.of(
        Arguments.of(
            randomUUID().toString(), randomUUID().toString(), BigDecimal.ZERO, description),
        Arguments.of(
            randomUUID().toString(), randomUUID().toString(), new BigDecimal(-100), description),
        Arguments.of(
            randomUUID().toString(), randomUUID().toString(), new BigDecimal(100), description)
    );
  }
}
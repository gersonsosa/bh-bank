package com.blueharvest.transaction.api.model;

import java.io.Serializable;
import javax.validation.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class TransactionRequest implements Serializable {

  @NotEmpty
  private String accountId;
  @NumberFormat(style = Style.CURRENCY)
  private String amount;
  @NotEmpty
  private String description;

  public TransactionRequest() {
  }

  public TransactionRequest(String accountId, String amount, String description) {
    this.accountId = accountId;
    this.amount = amount;
    this.description = description;
  }

  public String getAccountId() {
    return accountId;
  }

  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}

package com.blueharvest.transaction.service.impl;

import com.blueharvest.transaction.model.Transaction;
import com.blueharvest.transaction.repository.AccountRepository;
import com.blueharvest.transaction.repository.TransactionCreationException;
import com.blueharvest.transaction.repository.TransactionRepository;
import com.blueharvest.transaction.service.TransactionMessageSender;
import com.blueharvest.transaction.service.TransactionService;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;

public class TransactionServiceImpl implements TransactionService {

  private final Clock clock;
  private final AccountRepository accountRepository;
  private final TransactionRepository transactionRepository;
  private final TransactionMessageSender transactionMessageSender;

  public TransactionServiceImpl(
      Clock clock, AccountRepository accountRepository,
      TransactionRepository transactionRepository,
      TransactionMessageSender transactionMessageSender) {
    this.clock = clock;
    this.accountRepository = accountRepository;
    this.transactionRepository = transactionRepository;
    this.transactionMessageSender = transactionMessageSender;
  }

  @Override
  public Transaction create(String accountId, BigDecimal amount, String description)
      throws TransactionCreationException {

    if (!accountRepository.exists(accountId)) {
      throw new IllegalArgumentException(
          String.format("Account with id %s, doesn't exists", accountId));
    }
    final Transaction transaction = new Transaction();
    transaction.setAccountId(accountId);
    transaction.setAmount(amount);
    transaction.setDescription(description);
    transaction.setCreated(LocalDateTime.now(clock));
    final Transaction created = transactionRepository.save(transaction);
    transactionMessageSender.onTransactionCreated(created);
    return created;
  }

  @Override
  public List<Transaction> findByAccountId(String accountId) {
    return transactionRepository.findByAccountId(accountId);
  }
}

package com.blueharvest.transaction.repository;

public interface AccountRepository {

  /**
   * Returns whether the account with the given id exists or not
   * @param accountId account id to verify
   * @return true if account exits false otherwise
   */
  boolean exists(String accountId) throws TransactionCreationException;
}

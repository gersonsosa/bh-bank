package com.blueharvest.transaction.repository;

public class TransactionCreationException extends Exception {

  public TransactionCreationException(Throwable parent) {
    super(parent);
  }
}

package com.blueharvest.transaction.service;

import com.blueharvest.transaction.model.Transaction;

public interface TransactionMessageSender {

  void onTransactionCreated(Transaction transaction);

}

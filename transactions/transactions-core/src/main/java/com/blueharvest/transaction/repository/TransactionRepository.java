package com.blueharvest.transaction.repository;

import com.blueharvest.transaction.model.Transaction;
import java.util.List;

public interface TransactionRepository {

  /**
   * Persists a transaction
   *
   * @param transaction the transaction to persist
   */
  Transaction save(Transaction transaction);

  /**
   * Retrieve transactions for given account id
   *
   * @return a list of transactions for the given account id
   */
  List<Transaction> findByAccountId(String accountId);

}

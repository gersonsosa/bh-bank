package com.blueharvest.transaction.service;

import com.blueharvest.transaction.model.Transaction;
import com.blueharvest.transaction.repository.TransactionCreationException;
import java.math.BigDecimal;
import java.util.List;

public interface TransactionService {

  /**
   * Creates a new transaction for the given account id The amount can be positive -> credit or
   * negative -> debit
   *
   * @param accountId the account id to add the transaction
   * @param amount the amount of the transaction
   * @param description a description for the transaction
   * @return the created transaction
   */
  Transaction create(String accountId, BigDecimal amount, String description)
      throws TransactionCreationException;

  /**
   * Retrieves a list of the transactions for the given account id
   */
  List<Transaction> findByAccountId(String accountId);

}

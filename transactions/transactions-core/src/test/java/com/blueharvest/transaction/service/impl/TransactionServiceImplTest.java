package com.blueharvest.transaction.service.impl;

import static java.util.UUID.randomUUID;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.blueharvest.transaction.model.Transaction;
import com.blueharvest.transaction.repository.AccountRepository;
import com.blueharvest.transaction.repository.TransactionCreationException;
import com.blueharvest.transaction.repository.TransactionRepository;
import com.blueharvest.transaction.service.TransactionMessageSender;
import com.blueharvest.transaction.service.TransactionService;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class TransactionServiceImplTest {

  private TransactionService subject;
  @Mock
  private Clock clock;
  @Mock
  private TransactionRepository transactionRepository;
  @Mock
  private AccountRepository accountRepository;
  @Mock
  private TransactionMessageSender transactionMessageSender;
  private LocalDateTime now;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new TransactionServiceImpl(clock, accountRepository, transactionRepository,
        transactionMessageSender);

    final Instant instant = Instant.now();
    final ZoneId zoneId = ZoneId.systemDefault();
    this.now = LocalDateTime.ofInstant(instant, zoneId);

    when(clock.instant()).thenReturn(instant);
    when(clock.getZone()).thenReturn(zoneId);
  }

  @ParameterizedTest
  @MethodSource("createTransactionProvider")
  void shouldCreateTransaction(String accountId, BigDecimal amount, String description,
      String transactionId) throws TransactionCreationException {
    Transaction expected = new Transaction(transactionId, amount, description, accountId, now);
    when(accountRepository.exists(accountId)).thenReturn(true);
    when(transactionRepository.save(refEq(expected, "id"))).thenReturn(expected);

    Transaction transaction = subject.create(accountId, amount, description);

    assertThat(transaction).isEqualToComparingFieldByField(expected);
  }

  @Test
  void shouldThrowExceptionIfAccountDoesNotExists() throws TransactionCreationException {
    String accountId = randomUUID().toString();
    when(accountRepository.exists(accountId)).thenReturn(false);

    assertThatExceptionOfType(IllegalArgumentException.class)
        .isThrownBy(() -> subject
            .create(accountId, new BigDecimal(RandomUtils.nextFloat()), randomAlphabetic(255)));
  }

  @Test
  void shouldThrowTransactionCreationExceptionIfAccountRepositoryQueryFails()
      throws TransactionCreationException {
    String accountId = UUID.randomUUID().toString();
    when(accountRepository.exists(accountId))
        .thenThrow(new TransactionCreationException(new Exception()));

    assertThatExceptionOfType(TransactionCreationException.class)
        .isThrownBy(() -> subject
            .create(accountId, new BigDecimal(RandomUtils.nextFloat()), randomAlphabetic(255)));
  }

  @Test
  void shouldRetrieveListOfTransactionsForAccountId() {
    final String accountId = UUID.randomUUID().toString();
    final List<Transaction> expected = Arrays.asList(
        createRandomTransaction(), createRandomTransaction());
    when(transactionRepository.findByAccountId(accountId)).thenReturn(expected);

    final List<Transaction> transactions = subject.findByAccountId(accountId);

    assertThat(transactions).isEqualTo(expected);
  }

  @ParameterizedTest
  @MethodSource("createTransactionProvider")
  void shouldSendTransactionToMessageQueueWhenCreated(String accountId, BigDecimal amount,
      String description, String transactionId) throws TransactionCreationException {
    final Transaction expected = new Transaction(transactionId, amount, description, accountId, now);
    when(accountRepository.exists(accountId)).thenReturn(true);
    when(transactionRepository.save(refEq(expected, "id"))).thenReturn(expected);

    subject.create(accountId, amount, description);

    verify(transactionMessageSender).onTransactionCreated(expected);
  }

  private Transaction createRandomTransaction() {
    return new Transaction(UUID.randomUUID().toString(),
        new BigDecimal(RandomUtils.nextFloat()), randomAlphabetic(255),
        UUID.randomUUID().toString(), LocalDateTime.now());
  }

  private static Stream<Arguments> createTransactionProvider() {
    return Stream.of(
        Arguments.of(
            randomUUID().toString(), new BigDecimal(100L), randomAlphabetic(255),
            randomUUID().toString()),
        Arguments.of(
            randomUUID().toString(), new BigDecimal(-100L), randomAlphabetic(255),
            randomUUID().toString()),
        Arguments.of(randomUUID().toString(), BigDecimal.ZERO, randomAlphabetic(255),
            randomUUID().toString())
    );
  }
}
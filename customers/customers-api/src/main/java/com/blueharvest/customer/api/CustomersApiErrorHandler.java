package com.blueharvest.customer.api;

import com.blueharvest.customer.service.CustomerQueryException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomersApiErrorHandler {

  private static final String CUSTOMER_QUERY_MESSAGE = "The customer information cannot be retrieved, please try again later";

  @ExceptionHandler(CustomerQueryException.class)
  public ResponseEntity<String> handleException(CustomerQueryException exception) {
    return new ResponseEntity<>(CUSTOMER_QUERY_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
  }

}

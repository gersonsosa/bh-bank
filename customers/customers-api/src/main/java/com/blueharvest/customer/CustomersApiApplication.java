package com.blueharvest.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@ComponentScan(
    basePackages = {
        "com.blueharvest.accountview.api.client",
        "com.blueharvest.customer"
    }
)
@EnableSwagger2
@SpringBootApplication
public class CustomersApiApplication {

  public static void main(String[] args) {
    SpringApplication.run(CustomersApiApplication.class, args);
  }

}

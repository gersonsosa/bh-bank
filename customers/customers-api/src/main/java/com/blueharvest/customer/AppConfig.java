package com.blueharvest.customer;

import com.blueharvest.customer.repository.AccountRepository;
import com.blueharvest.customer.repository.CustomerRepository;
import com.blueharvest.customer.service.CustomerService;
import com.blueharvest.customer.service.impl.CustomerServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class AppConfig {

  @Bean
  public CustomerService customerService(CustomerRepository customerRepository,
      AccountRepository accountRepository) {
    return new CustomerServiceImpl(customerRepository, accountRepository);
  }

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.regex("/customers.*"))
        .build()
        .apiInfo(apiInfo());
  }

  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title("Customers API")
        .description("Api to handle all Customers operations")
        .build();
  }

}

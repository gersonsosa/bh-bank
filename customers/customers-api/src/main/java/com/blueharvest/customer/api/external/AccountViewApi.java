package com.blueharvest.customer.api.external;

import com.blueharvest.account.model.Account;
import com.blueharvest.accountview.api.client.AccountViewApiClient;
import com.blueharvest.accountview.api.client.AccountViewClientException;
import com.blueharvest.customer.repository.AccountQueryException;
import com.blueharvest.customer.repository.AccountRepository;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class AccountViewApi implements AccountRepository {

  private final AccountViewApiClient accountViewApiClient;

  AccountViewApi(
      AccountViewApiClient accountViewApiClient) {
    this.accountViewApiClient = accountViewApiClient;
  }

  @Override
  public List<Account> findByCustomerId(String customerId) throws AccountQueryException {
    try {
      return accountViewApiClient.findByCustomerId(customerId);
    } catch (AccountViewClientException e) {
      throw new AccountQueryException(e);
    }
  }
}

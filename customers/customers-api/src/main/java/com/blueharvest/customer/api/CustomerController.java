package com.blueharvest.customer.api;

import com.blueharvest.customer.model.Customer;
import com.blueharvest.customer.service.CustomerQueryException;
import com.blueharvest.customer.service.CustomerService;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("customers")
class CustomerController {

  private final CustomerService customerService;

  CustomerController(CustomerService customerService) {
    this.customerService = customerService;
  }

  @GetMapping
  ResponseEntity<List<Customer>> findAll() {
    return ResponseEntity.ok(customerService.findAll());
  }

  @GetMapping("/{customerId}")
  ResponseEntity<Customer> findById(@PathVariable String customerId)
      throws CustomerQueryException {
    return ResponseEntity.of(customerService.findById(customerId));
  }

}

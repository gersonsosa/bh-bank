package com.blueharvest.customer.api;

import static com.blueharvest.customer.api.RandomModelGenerator.createRandomCustomer;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.when;

import com.blueharvest.customer.model.Customer;
import com.blueharvest.customer.service.CustomerQueryException;
import com.blueharvest.customer.service.CustomerService;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

class CustomerControllerTest {

  private CustomerController subject;
  @Mock
  private CustomerService customerService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new CustomerController(customerService);
  }

  @Test
  void shouldReturnCustomerInformation() throws CustomerQueryException {
    final String customerId = UUID.randomUUID().toString();
    final Customer expected = createRandomCustomer(customerId);
    when(customerService.findById(customerId)).thenReturn(Optional.of(expected));

    final ResponseEntity<Customer> responseEntity = subject.findById(customerId);

    assertThat(responseEntity).satisfies(response -> {
      assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
      assertThat(response.getBody()).isEqualToComparingFieldByFieldRecursively(expected);
    });
  }

  @Test
  void shouldReturnNotFoundHttpCodeWhenCustomerDoesNotExists() throws CustomerQueryException {
    final String customerId = UUID.randomUUID().toString();
    when(customerService.findById(customerId)).thenReturn(Optional.empty());

    final ResponseEntity<Customer> responseEntity = subject
        .findById(customerId);

    assertThat(responseEntity).satisfies(
        response -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND));
  }

  @Test
  void shouldThrowCustomerQueryExceptionWhenExternalServiceFails() throws CustomerQueryException {
    final String customerId = UUID.randomUUID().toString();
    when(customerService.findById(customerId))
        .thenThrow(new CustomerQueryException(new Exception()));

    assertThatExceptionOfType(CustomerQueryException.class)
        .isThrownBy(() -> subject.findById(customerId));
  }

  @Test
  void shouldReturnAllCustomers() {
    final List<Customer> expected = Arrays.asList(
        createRandomCustomer(UUID.randomUUID().toString()),
        createRandomCustomer(UUID.randomUUID().toString()),
        createRandomCustomer(UUID.randomUUID().toString()));
    when(customerService.findAll()).thenReturn(expected);

    final ResponseEntity<List<Customer>> customers = subject.findAll();

    assertThat(customers).satisfies(response -> {
          assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
          assertThat(response.getBody()).usingElementComparatorIgnoringFields("accounts")
              .isEqualTo(expected);
        }
    );
  }
}
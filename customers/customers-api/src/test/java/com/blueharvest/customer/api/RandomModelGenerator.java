package com.blueharvest.customer.api;

import com.blueharvest.account.model.Account;
import com.blueharvest.customer.model.Customer;
import com.blueharvest.transaction.model.Transaction;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.UUID;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

public class RandomModelGenerator {

  public static Customer createRandomCustomer(String customerId) {
    return new Customer(customerId,
        RandomStringUtils.randomAlphabetic(12),
        RandomStringUtils.randomAlphabetic(12),
        Arrays.asList(createRandomAccount(), createRandomAccount(), createRandomAccount()));
  }

  public static Account createRandomAccount() {
    return new Account(
        UUID.randomUUID().toString(),
        new BigDecimal(RandomUtils.nextFloat()),
        UUID.randomUUID().toString(),
        LocalDateTime.now(),
        Arrays.asList(createRandomTransaction(), createRandomTransaction()));
  }

  static Transaction createRandomTransaction() {
    return new Transaction(UUID.randomUUID().toString(), new BigDecimal(RandomUtils.nextFloat()),
        RandomStringUtils.randomAlphabetic(255), UUID.randomUUID().toString(), LocalDateTime.now());
  }

}

package com.blueharvest.customer.api.external;

import static com.blueharvest.customer.api.RandomModelGenerator.createRandomAccount;

import com.blueharvest.account.model.Account;
import com.blueharvest.accountview.api.client.AccountViewApiClient;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

class AccountViewApiTest {

  private AccountViewApi subject;
  @Mock
  private AccountViewApiClient accountViewApiClient;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new AccountViewApi(accountViewApiClient);
  }

  @Test
  void shouldReturnAccountData() throws Exception {
    String customerId = UUID.randomUUID().toString();
    List<Account> accounts = Arrays.asList(createRandomAccount(), createRandomAccount());
    Mockito.when(accountViewApiClient.findByCustomerId(customerId)).thenReturn(accounts);

    final List<Account> result = subject.findByCustomerId(customerId);

    Assertions.assertThat(result).isEqualTo(accounts);
  }
}
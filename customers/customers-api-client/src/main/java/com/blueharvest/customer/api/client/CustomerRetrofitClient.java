package com.blueharvest.customer.api.client;

import retrofit2.Call;
import retrofit2.http.HEAD;
import retrofit2.http.Path;

public interface CustomerRetrofitClient {

  @HEAD("customers/{customerId}")
  Call<Void> exists(@Path("customerId") String customerId);

}

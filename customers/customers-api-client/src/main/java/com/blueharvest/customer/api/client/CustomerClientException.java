package com.blueharvest.customer.api.client;

public class CustomerClientException extends Exception {

  private static final String CLIENT_ERROR_MESSAGE = "An error occurred invoking the customers-api with response code: %d and message %s";

  public CustomerClientException(int code, String message) {
    super(String.format(CLIENT_ERROR_MESSAGE, code, message));
  }

  public CustomerClientException(Throwable throwable) {
    super(throwable);
  }
}

package com.blueharvest.customer.api.client;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Component
public class CustomerApiClient {

  private final CustomerRetrofitClient customerRetrofitClient;

  public CustomerApiClient(@Value("${customers.api.url}") String customersApiUrl) {
    final Retrofit retrofit = new Retrofit.Builder().baseUrl(customersApiUrl).addConverterFactory(
        GsonConverterFactory.create()).build();
    customerRetrofitClient = retrofit.create(CustomerRetrofitClient.class);
  }

  public Boolean exists(String customerId) throws CustomerClientException {
    try {
      final Response<Void> response = customerRetrofitClient.exists(customerId).execute();
      return response.isSuccessful();
    } catch (IOException e) {
      throw new CustomerClientException(e);
    }
  }
}

package com.blueharvest.customer.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.when;

import com.blueharvest.account.model.Account;
import com.blueharvest.customer.model.Customer;
import com.blueharvest.customer.repository.AccountQueryException;
import com.blueharvest.customer.repository.AccountRepository;
import com.blueharvest.customer.repository.CustomerRepository;
import com.blueharvest.customer.service.CustomerQueryException;
import com.blueharvest.customer.service.CustomerService;
import com.blueharvest.transaction.model.Transaction;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class CustomerServiceImplTest {

  private CustomerService subject;
  @Mock
  private CustomerRepository customerRepository;
  @Mock
  private AccountRepository accountRepository;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new CustomerServiceImpl(customerRepository, accountRepository);
  }

  @Test
  void shouldRetrieveCustomerDataFromRepository() throws CustomerQueryException {
    final String customerId = UUID.randomUUID().toString();
    final Customer expected = createRandomCustomer(customerId);
    when(customerRepository.findById(customerId)).thenReturn(Optional.of(expected));

    final Optional<Customer> customerOptional = subject.findById(customerId);

    assertThat(customerOptional).hasValueSatisfying(
        customer -> assertThat(customer).isEqualToComparingFieldByFieldRecursively(expected));
  }

  @Test
  void shouldThrowCustomerQueryExceptionWhenAccountServiceFails() throws Exception {
    final String customerId = UUID.randomUUID().toString();
    when(customerRepository.findById(customerId))
        .thenReturn(Optional.of(createRandomCustomer(customerId)));
    when(accountRepository.findByCustomerId(customerId))
        .thenThrow(new AccountQueryException(new Exception()));

    assertThatExceptionOfType(CustomerQueryException.class)
        .isThrownBy(() -> subject.findById(customerId));
  }

  @Test
  void shouldRetrieveAllCustomers() {
    final List<Customer> customerList = Arrays.asList(
        createRandomCustomer(UUID.randomUUID().toString()),
        createRandomCustomer(UUID.randomUUID().toString()));
    when(customerRepository.findAll()).thenReturn(customerList);
    final List<Customer> customers = subject.findAll();

    assertThat(customers).isEqualTo(customerList);
  }

  private Customer createRandomCustomer(String customerId) {
    return new Customer(customerId, RandomStringUtils.randomAlphabetic(12),
        RandomStringUtils.randomAlphabetic(12),
        Arrays.asList(createRandomAccount(), createRandomAccount(), createRandomAccount()));
  }

  private Account createRandomAccount() {
    return new Account(UUID.randomUUID().toString(), new BigDecimal(RandomUtils.nextFloat()),
        UUID.randomUUID().toString(), LocalDateTime.now(),
        Arrays.asList(createRandomTransaction(), createRandomTransaction()));
  }

  private Transaction createRandomTransaction() {
    return new Transaction(UUID.randomUUID().toString(), new BigDecimal(RandomUtils.nextFloat()),
        RandomStringUtils.randomAlphabetic(255), UUID.randomUUID().toString(), LocalDateTime.now());
  }
}
package com.blueharvest.customer.service;

import com.blueharvest.customer.model.Customer;
import java.util.List;
import java.util.Optional;

public interface CustomerService {

  /**
   * Retrieves the customer including the accounts and transactions
   *
   * @param customerId customer unique identifier
   */
  Optional<Customer> findById(String customerId) throws CustomerQueryException;

  List<Customer> findAll();
}

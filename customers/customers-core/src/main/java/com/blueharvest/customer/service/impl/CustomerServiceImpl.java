package com.blueharvest.customer.service.impl;

import com.blueharvest.customer.model.Customer;
import com.blueharvest.customer.repository.AccountQueryException;
import com.blueharvest.customer.repository.AccountRepository;
import com.blueharvest.customer.repository.CustomerRepository;
import com.blueharvest.customer.service.CustomerQueryException;
import com.blueharvest.customer.service.CustomerService;
import java.util.List;
import java.util.Optional;

public class CustomerServiceImpl implements CustomerService {

  private final CustomerRepository customerRepository;
  private final AccountRepository accountRepository;

  public CustomerServiceImpl(
      CustomerRepository customerRepository,
      AccountRepository accountRepository) {
    this.customerRepository = customerRepository;
    this.accountRepository = accountRepository;
  }

  @Override
  public Optional<Customer> findById(String customerId) throws CustomerQueryException {
    final Optional<Customer> customerOptional = customerRepository.findById(customerId);
    if (customerOptional.isPresent()) {
      Customer customer = customerOptional.get();
      try {
        customer.setAccounts(accountRepository.findByCustomerId(customerId));
      } catch (AccountQueryException e) {
        throw new CustomerQueryException(e);
      }
      return Optional.of(customer);
    } else {
      return Optional.empty();
    }
  }

  @Override
  public List<Customer> findAll() {
    return customerRepository.findAll();
  }
}

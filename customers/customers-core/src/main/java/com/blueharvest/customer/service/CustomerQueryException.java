package com.blueharvest.customer.service;

public class CustomerQueryException extends Exception {

  public CustomerQueryException(Throwable throwable) {
    super(throwable);
  }
}

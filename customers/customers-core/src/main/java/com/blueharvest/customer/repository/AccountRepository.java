package com.blueharvest.customer.repository;

import com.blueharvest.account.model.Account;
import java.util.List;

public interface AccountRepository {

  /**
   * Retrieves all accounts and transactions for the given customer id
   *
   * @param customerId the customer unique identifier
   * @return A list of accounts with transactions
   */
  List<Account> findByCustomerId(String customerId) throws AccountQueryException;

}

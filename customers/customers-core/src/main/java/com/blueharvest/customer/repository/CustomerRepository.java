package com.blueharvest.customer.repository;

import com.blueharvest.customer.model.Customer;
import java.util.List;
import java.util.Optional;

public interface CustomerRepository {

  /**
   * Retrieves the customer data
   *
   * @param customerId customer unique identifier
   * @return An optional of the customer with the given id or an empty optional
   */
  Optional<Customer> findById(String customerId);

  List<Customer> findAll();
}

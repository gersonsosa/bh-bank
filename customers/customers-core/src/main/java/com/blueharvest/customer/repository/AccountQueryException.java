package com.blueharvest.customer.repository;

public class AccountQueryException extends Exception {

  public AccountQueryException(Throwable throwable) {
    super(throwable);
  }
}

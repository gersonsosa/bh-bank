package com.blueharvest.customer.repository.mapper;

import static com.googlecode.jmapper.api.JMapperAPI.mappedClass;

import com.blueharvest.customer.model.Customer;
import com.blueharvest.customer.repository.jpa.model.CustomerEntity;
import com.googlecode.jmapper.JMapper;
import com.googlecode.jmapper.api.Global;
import com.googlecode.jmapper.api.JMapperAPI;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class CustomerMapper {

  private final JMapper<Customer, CustomerEntity> customerJMapper;

  CustomerMapper() {
    final JMapperAPI jMapperAPI = new JMapperAPI().add(
        mappedClass(Customer.class).add(new Global().excludedAttributes("accounts")));
    customerJMapper = new JMapper<>(Customer.class, CustomerEntity.class, jMapperAPI);
  }

  public Customer map(CustomerEntity entity) {
    return customerJMapper.getDestination(entity);
  }

  public List<Customer> map(List<CustomerEntity> entities) {
    return entities.parallelStream().map(this::map).collect(Collectors.toList());
  }
}

package com.blueharvest.customer.repository.impl;

import com.blueharvest.customer.model.Customer;
import com.blueharvest.customer.repository.CustomerRepository;
import com.blueharvest.customer.repository.jpa.CustomerJpaRepository;
import com.blueharvest.customer.repository.mapper.CustomerMapper;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

  private final CustomerJpaRepository customerJpaRepository;
  private final CustomerMapper customerMapper;

  CustomerRepositoryImpl(
      CustomerJpaRepository customerJpaRepository,
      CustomerMapper customerMapper) {
    this.customerJpaRepository = customerJpaRepository;
    this.customerMapper = customerMapper;
  }

  @Override
  public Optional<Customer> findById(String customerId) {
    return customerJpaRepository.findById(customerId).map(customerMapper::map);
  }

  @Override
  public List<Customer> findAll() {
    return customerMapper.map(customerJpaRepository.findAll());
  }
}

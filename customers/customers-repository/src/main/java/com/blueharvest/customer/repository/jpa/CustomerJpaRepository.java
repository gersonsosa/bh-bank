package com.blueharvest.customer.repository.jpa;

import com.blueharvest.customer.repository.jpa.model.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerJpaRepository extends JpaRepository<CustomerEntity, String> {

}

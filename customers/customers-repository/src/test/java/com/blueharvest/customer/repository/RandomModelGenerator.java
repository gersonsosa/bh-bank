package com.blueharvest.customer.repository;

import com.blueharvest.customer.model.Customer;
import com.blueharvest.customer.repository.jpa.model.CustomerEntity;
import org.apache.commons.lang3.RandomStringUtils;

public class RandomModelGenerator {

  public static CustomerEntity createRandomCustomerEntity(String customerId) {
    return new CustomerEntity(customerId,
        RandomStringUtils.randomAlphabetic(12),
        RandomStringUtils.randomAlphabetic(12));
  }

  public static Customer createRandomCustomer(String customerId) {
    return new Customer(customerId, RandomStringUtils.randomAlphabetic(12),
        RandomStringUtils.randomAlphabetic(12), null);
  }

}

package com.blueharvest.customer.repository.impl;

import static com.blueharvest.customer.repository.RandomModelGenerator.createRandomCustomerEntity;
import static org.assertj.core.api.Assertions.assertThat;

import com.blueharvest.customer.model.Customer;
import com.blueharvest.customer.repository.RepositoryTestConfig;
import com.blueharvest.customer.repository.jpa.model.CustomerEntity;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@ContextConfiguration(classes = RepositoryTestConfig.class)
@Tag("integration")
class CustomerRepositoryImplIntegrationTest {

  @Autowired
  private CustomerRepositoryImpl subject;

  @Autowired
  private TestEntityManager testEntityManager;

  @Test
  void shouldRetrieveCustomerDataById() {
    final String customerId = UUID.randomUUID().toString();
    final CustomerEntity expected = createRandomCustomerEntity(customerId);
    testEntityManager.persist(expected);
    final Optional<Customer> optionalCustomer = subject.findById(customerId);

    assertThat(optionalCustomer).hasValueSatisfying(
        customer -> assertThat(customer).isEqualToIgnoringGivenFields(expected, "accounts"));
  }

  @Test
  void shouldRetrieveAllCustomers() {
    final List<CustomerEntity> customerEntities = Arrays.asList(
        createRandomCustomerEntity(UUID.randomUUID().toString()),
        createRandomCustomerEntity(UUID.randomUUID().toString()));

    customerEntities.forEach(testEntityManager::persist);

    final List<Customer> all = subject.findAll();

    assertThat(all).usingElementComparatorIgnoringFields("accounts").isEqualTo(customerEntities);
  }
}
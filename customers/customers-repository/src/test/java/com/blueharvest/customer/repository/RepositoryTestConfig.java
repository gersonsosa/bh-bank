package com.blueharvest.customer.repository;

import com.blueharvest.customer.repository.impl.CustomerRepositoryImpl;
import com.blueharvest.customer.repository.mapper.CustomerMapper;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = {CustomerRepositoryImpl.class, CustomerMapper.class})
public class RepositoryTestConfig {
}

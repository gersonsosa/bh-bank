package com.blueharvest.customer.repository.impl;

import static com.blueharvest.customer.repository.RandomModelGenerator.createRandomCustomer;
import static com.blueharvest.customer.repository.RandomModelGenerator.createRandomCustomerEntity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.blueharvest.customer.model.Customer;
import com.blueharvest.customer.repository.CustomerRepository;
import com.blueharvest.customer.repository.jpa.CustomerJpaRepository;
import com.blueharvest.customer.repository.jpa.model.CustomerEntity;
import com.blueharvest.customer.repository.mapper.CustomerMapper;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class CustomerRepositoryImplTest {

  private CustomerRepository subject;
  @Mock
  private CustomerJpaRepository customerJpaRepository;
  @Mock
  private CustomerMapper customerMapper;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new CustomerRepositoryImpl(customerJpaRepository, customerMapper);
  }

  @Test
  void shouldRetrieveCustomerData() {
    final String customerId = UUID.randomUUID().toString();
    final CustomerEntity customerEntity = createRandomCustomerEntity(customerId);
    final Customer expected = createRandomCustomer(customerId);
    when(customerJpaRepository.findById(customerId)).thenReturn(Optional.of(customerEntity));
    when(customerMapper.map(customerEntity)).thenReturn(expected);

    final Optional<Customer> customerOptional = subject.findById(customerId);

    assertThat(customerOptional).hasValueSatisfying(
        customer -> assertThat(customer).isEqualToIgnoringGivenFields(expected));
  }

  @Test
  void shouldReturnOptionalEmptyWhenCustomerDoesNotExists() {
    final String customerId = UUID.randomUUID().toString();
    when(customerJpaRepository.findById(customerId)).thenReturn(Optional.empty());

    final Optional<Customer> customerOptional = subject.findById(customerId);

    assertThat(customerOptional).isEmpty();
  }

  @Test
  void shouldReturnAllCustomers() {
    final List<CustomerEntity> customerEntities = Arrays.asList(
        createRandomCustomerEntity(UUID.randomUUID().toString()),
        createRandomCustomerEntity(UUID.randomUUID().toString()));
    when(customerJpaRepository.findAll()).thenReturn(customerEntities);
    final List<Customer> customerList = Arrays.asList(
        createRandomCustomer(UUID.randomUUID().toString()),
        createRandomCustomer(UUID.randomUUID().toString()));
    when(customerMapper.map(customerEntities)).thenReturn(customerList);

    final List<Customer> all = subject.findAll();

    assertThat(all).isEqualTo(customerList);
  }
}
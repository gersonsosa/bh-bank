package com.blueharvest.customer.repository.mapper;

import static java.util.UUID.randomUUID;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;

import com.blueharvest.customer.model.Customer;
import com.blueharvest.customer.repository.jpa.model.CustomerEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class CustomerMapperTest {

  private CustomerMapper subject;

  @BeforeEach
  void setUp() {
    subject = new CustomerMapper();
  }

  @ParameterizedTest
  @MethodSource("customerProvider")
  void shouldMapCustomerEntityToCustomer(String customerId, String name, String surname) {
    final CustomerEntity source = new CustomerEntity(customerId, name, surname);
    final Customer customer = subject.map(source);

    assertThat(customer).isEqualToIgnoringGivenFields(source, "accounts");
  }

  @ParameterizedTest
  @MethodSource("customerProvider")
  void shouldMapCustomerEntityListToCustomers(String customerId, String name, String surname) {
    final List<CustomerEntity> source = Collections.singletonList(
        new CustomerEntity(customerId, name, surname));
    final List<Customer> customers = subject.map(source);

    assertThat(customers).usingElementComparatorIgnoringFields("accounts").isEqualTo(source);
  }

  private static Stream<Arguments> customerProvider() {
    return Stream.of(
        Arguments.of(randomUUID().toString(), null, randomAlphabetic(15)),
        Arguments.of(randomUUID().toString(), randomAlphabetic(15), null),
        Arguments.of(randomUUID().toString(), randomAlphabetic(10), randomAlphabetic(15))
    );
  }
}
package com.blueharvest.customer.model;

import com.blueharvest.account.model.Account;
import java.util.List;

public class Customer {

  private String id;
  private String name;
  private String surname;
  private List<Account> accounts;

  public Customer() {
  }

  public Customer(String id, String name, String surname,
      List<Account> accounts) {
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.accounts = accounts;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public List<Account> getAccounts() {
    return accounts;
  }

  public void setAccounts(List<Account> accounts) {
    this.accounts = accounts;
  }
}

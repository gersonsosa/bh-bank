package com.blueharvest.account.api;

import static com.blueharvest.account.StubGenerator.createRandomAccount;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.when;

import com.blueharvest.account.api.model.AccountRequest;
import com.blueharvest.account.api.model.AccountRequest.Builder;
import com.blueharvest.account.model.Account;
import com.blueharvest.account.service.AccountCreationException;
import com.blueharvest.account.service.AccountsService;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

class AccountControllerTest {

  private AccountController subject;
  @Mock
  private AccountsService accountService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new AccountController(accountService);
  }

  @ParameterizedTest
  @MethodSource("accountRequestProvider")
  void shouldCreateAccount(String customerId, String balance) throws AccountCreationException {
    final AccountRequest request = new Builder()
        .withCustomerId(customerId)
        .withInitialBalance(balance)
        .build();

    final BigDecimal initialBalance = new BigDecimal(balance);
    final Account expected = new Account();
    expected.setId(UUID.randomUUID().toString());
    expected.setCustomerId(customerId);
    expected.setBalance(initialBalance);
    when(accountService.create(customerId, initialBalance)).thenReturn(expected);

    final ResponseEntity<Account> responseEntity = subject.create(request);

    assertThat(responseEntity).satisfies(response -> {
      assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
      assertThat(response.getBody()).isEqualToComparingFieldByField(expected);
    });
  }

  @ParameterizedTest
  @MethodSource("accountRequestProvider")
  void shouldThrowAccountCreationExceptionErrorWhenAccountCreationFails(String customerId,
      String balance) throws AccountCreationException {
    final AccountRequest request = new Builder()
        .withCustomerId(customerId).withInitialBalance(balance).build();

    when(accountService.create(customerId, new BigDecimal(balance)))
        .thenThrow(new AccountCreationException(new Exception()));

    assertThatExceptionOfType(AccountCreationException.class)
        .isThrownBy(() -> subject.create(request));
  }

  @Test
  void shouldReturnAccountById() {
    final Account expected = createRandomAccount();
    when(accountService.findById(expected.getId())).thenReturn(Optional.of(expected));
    final ResponseEntity<Account> accountResponseEntity = subject.findById(expected.getId());

    assertThat(accountResponseEntity).satisfies(response -> {
      assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
      assertThat(response.getBody()).isEqualTo(expected);
    });
  }

  private static Stream<Arguments> accountRequestProvider() {
    return Stream.of(
        Arguments.of(UUID.randomUUID().toString(), "0"),
        Arguments.of(UUID.randomUUID().toString(), "1000"),
        Arguments.of(UUID.randomUUID().toString(), "-1000")
    );
  }
}
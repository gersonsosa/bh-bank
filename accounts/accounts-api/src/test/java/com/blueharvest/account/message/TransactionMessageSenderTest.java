package com.blueharvest.account.message;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.blueharvest.transaction.api.model.TransactionRequest;
import java.math.BigDecimal;
import java.util.UUID;
import java.util.stream.Stream;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

class TransactionMessageSenderTest {

  private TransactionMessageSender subject;
  @Mock
  private RabbitTemplate rabbitTemplate;
  @Mock
  private Exchange transactionExchange;
  @Captor
  private ArgumentCaptor<TransactionRequest> transactionRequestArgumentCaptor;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new TransactionMessageSender(rabbitTemplate, transactionExchange);
  }

  @ParameterizedTest
  @MethodSource("transactionRequestProvider")
  void shouldSendTransactionCreationRequestToMessageBroker(String accountId, BigDecimal amount,
      String description) {
    final String exchange = "transactionExchange";
    when(this.transactionExchange.getName()).thenReturn(exchange);
    subject.create(accountId, amount, description);

    verify(rabbitTemplate)
        .convertAndSend(eq(exchange), eq("transaction.create"),
            transactionRequestArgumentCaptor.capture());

    assertThat(transactionRequestArgumentCaptor.getValue()).satisfies(request -> {
      assertThat(request.getAccountId()).isEqualTo(accountId);
      assertThat(amount.toString().equals(request.getAmount())).isTrue();
      assertThat(request.getDescription()).isEqualTo(description);
    });
  }

  private static Stream<Arguments> transactionRequestProvider() {
    return Stream.of(
        Arguments.of(UUID.randomUUID().toString(), new BigDecimal(-100.1F),
            RandomStringUtils.randomAlphabetic(255)),
        Arguments.of(UUID.randomUUID().toString(), new BigDecimal(102.1F),
            RandomStringUtils.randomAlphabetic(255)),
        Arguments.of(UUID.randomUUID().toString(), BigDecimal.ZERO,
            RandomStringUtils.randomAlphabetic(255))
    );
  }
}
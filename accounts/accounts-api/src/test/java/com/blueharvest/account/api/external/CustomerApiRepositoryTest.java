package com.blueharvest.account.api.external;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.when;

import com.blueharvest.account.service.AccountCreationException;
import com.blueharvest.customer.api.client.CustomerApiClient;
import com.blueharvest.customer.api.client.CustomerClientException;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class CustomerApiRepositoryTest {

  private CustomerApiRepository subject;
  @Mock
  private CustomerApiClient customerApiClient;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new CustomerApiRepository(customerApiClient);
  }

  @ParameterizedTest
  @ValueSource(strings = {"true", "false"})
  void shouldQueryCustomerApiToVerifyCustomerExistence(Boolean b) throws Exception {
    String customerId = UUID.randomUUID().toString();
    when(customerApiClient.exists(customerId)).thenReturn(b);
    assertThat(subject.exists(customerId)).isEqualTo(b);
  }

  @Test
  void shouldThrowAccountCreationExceptionWhenCustomerApiFails() throws CustomerClientException {
    String customerId = UUID.randomUUID().toString();
    when(customerApiClient.exists(customerId))
        .thenThrow(new CustomerClientException(new Exception()));
    assertThatExceptionOfType(AccountCreationException.class)
        .isThrownBy(() -> subject.exists(customerId));
  }
}
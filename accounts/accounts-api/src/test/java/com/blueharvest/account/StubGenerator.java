package com.blueharvest.account;

import com.blueharvest.account.model.Account;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.UUID;
import org.apache.commons.lang3.RandomUtils;

public class StubGenerator {

  public static Account createRandomAccount() {
    return new Account(UUID.randomUUID().toString(),
        new BigDecimal(RandomUtils.nextFloat()),
        UUID.randomUUID().toString(),
        LocalDateTime.now(),
        Collections.emptyList());
  }

}

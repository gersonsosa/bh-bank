package com.blueharvest.account.message;

import static com.blueharvest.account.StubGenerator.createRandomAccount;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.blueharvest.account.model.Account;
import com.blueharvest.account.service.AccountMessageSender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

class AccountMessageSenderImplTest {

  private AccountMessageSender subject;
  @Mock
  private RabbitTemplate rabbitTemplate;
  @Mock
  private Exchange accountExchange;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new AccountMessageSenderImpl(rabbitTemplate, accountExchange);
  }

  @Test
  void shouldSendAccountCreatedToMessageBroker() {
    final String exchangeName = "accountCreatedExchange";
    when(accountExchange.getName()).thenReturn(exchangeName);
    final Account account = createRandomAccount();
    subject.sendAccountCreated(account);

    verify(rabbitTemplate).convertAndSend(exchangeName, "account.created", account);
  }
}
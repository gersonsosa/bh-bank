package com.blueharvest.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@ComponentScan(
    basePackages = {
        "com.blueharvest.customer.api.client",
        "com.blueharvest.account"
    }
)
@EnableSwagger2
@SpringBootApplication
public class AccountsApiApplication {

  public static void main(String[] args) {
    SpringApplication.run(AccountsApiApplication.class, args);
  }

}

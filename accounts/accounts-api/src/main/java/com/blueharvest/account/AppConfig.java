package com.blueharvest.account;

import com.blueharvest.account.repository.AccountRepository;
import com.blueharvest.account.repository.CustomerRepository;
import com.blueharvest.account.service.AccountMessageSender;
import com.blueharvest.account.service.AccountsService;
import com.blueharvest.account.service.TransactionService;
import com.blueharvest.account.service.impl.AccountsServiceImpl;
import java.time.Clock;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@SuppressWarnings("unused")
@Configuration
public class AppConfig {

  @Bean
  public Clock clock() {
    return Clock.systemDefaultZone();
  }

  @Bean
  public AccountsService accountsService(Clock clock, CustomerRepository customerRepository,
      AccountRepository accountRepository, TransactionService transactionService,
      AccountMessageSender accountMessageSender) {
    return new AccountsServiceImpl(clock, customerRepository, accountRepository, transactionService,
        accountMessageSender);
  }

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.regex("/accounts.*"))
        .build()
        .apiInfo(apiInfo());
  }

  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title("Accounts API")
        .description("Api to handle all Accounts operations")
        .build();
  }

}

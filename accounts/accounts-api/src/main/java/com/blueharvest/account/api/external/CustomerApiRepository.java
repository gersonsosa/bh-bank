package com.blueharvest.account.api.external;

import com.blueharvest.account.repository.CustomerRepository;
import com.blueharvest.account.service.AccountCreationException;
import com.blueharvest.customer.api.client.CustomerApiClient;
import com.blueharvest.customer.api.client.CustomerClientException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component
public class CustomerApiRepository implements CustomerRepository {

  private final CustomerApiClient customerApiClient;

  CustomerApiRepository(
      CustomerApiClient customerApiClient) {
    this.customerApiClient = customerApiClient;
  }

  @Override
  public boolean exists(String customerId) throws AccountCreationException {
    try {
      return customerApiClient.exists(customerId);
    } catch (CustomerClientException e) {
      throw new AccountCreationException(e);
    }
  }
}

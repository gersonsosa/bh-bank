package com.blueharvest.account.message;

import com.blueharvest.account.service.TransactionService;
import com.blueharvest.transaction.api.model.TransactionRequest;
import java.math.BigDecimal;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class TransactionMessageSender implements TransactionService {

  private final RabbitTemplate rabbitTemplate;
  private final Exchange transactionExchange;

  TransactionMessageSender(
      RabbitTemplate rabbitTemplate, Exchange transactionExchange) {
    this.rabbitTemplate = rabbitTemplate;
    this.transactionExchange = transactionExchange;
  }

  @Override
  public void create(String accountId, BigDecimal amount, String description) {
    rabbitTemplate.convertAndSend(transactionExchange.getName(), "transaction.create",
        new TransactionRequest(accountId, amount.toString(), description));
  }
}

package com.blueharvest.account.message;

import com.blueharvest.account.model.Account;
import com.blueharvest.account.service.AccountMessageSender;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class AccountMessageSenderImpl implements AccountMessageSender {

  private final RabbitTemplate rabbitTemplate;
  private final Exchange accountExchange;

  AccountMessageSenderImpl(RabbitTemplate rabbitTemplate, Exchange accountExchange) {
    this.rabbitTemplate = rabbitTemplate;
    this.accountExchange = accountExchange;
  }

  @Override
  public void sendAccountCreated(Account account) {
    rabbitTemplate.convertAndSend(accountExchange.getName(), "account.created", account);
  }
}

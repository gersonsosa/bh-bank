package com.blueharvest.account.message;

import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BrokerConfig {

  @Bean
  public Exchange accountExchange() {
    return new TopicExchange("accountCreatedExchange");
  }

  @Bean
  public Exchange transactionExchange() {
    return new TopicExchange("transactionExchange");
  }

}

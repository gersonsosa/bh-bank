package com.blueharvest.account.api;

import com.blueharvest.account.api.model.AccountRequest;
import com.blueharvest.account.model.Account;
import com.blueharvest.account.service.AccountCreationException;
import com.blueharvest.account.service.AccountsService;
import java.math.BigDecimal;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("accounts")
class AccountController {

  private final AccountsService accountsService;

  AccountController(AccountsService accountsService) {
    this.accountsService = accountsService;
  }

  @CrossOrigin
  @PostMapping
  ResponseEntity<Account> create(@RequestBody AccountRequest request)
      throws AccountCreationException {
    return ResponseEntity.ok(
        accountsService.create(request.getCustomerId(), new BigDecimal(request.getInitialBalance()))
    );
  }

  @GetMapping("/{accountId}")
  ResponseEntity<Account> findById(@PathVariable String accountId) {
    return ResponseEntity.of(accountsService.findById(accountId));
  }

}

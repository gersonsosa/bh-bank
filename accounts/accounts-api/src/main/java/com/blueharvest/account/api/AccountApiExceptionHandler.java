package com.blueharvest.account.api;

import com.blueharvest.account.service.AccountCreationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AccountApiExceptionHandler {

  private static final String ACCOUNT_CREATION_EXCEPTION_MESSAGE = "The request cannot be process now, please try again";
  private static final String INVALID_PARAMETERS = "The request contains invalid parameters";

  @ExceptionHandler(AccountCreationException.class)
  public ResponseEntity<String> handleException(AccountCreationException e) {
    return new ResponseEntity<>(ACCOUNT_CREATION_EXCEPTION_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(IllegalArgumentException.class)
  public ResponseEntity<String> handleException(IllegalArgumentException exception) {
    return new ResponseEntity<>(INVALID_PARAMETERS, HttpStatus.BAD_REQUEST);
  }

}

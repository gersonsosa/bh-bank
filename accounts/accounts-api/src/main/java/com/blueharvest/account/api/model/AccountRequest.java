package com.blueharvest.account.api.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class AccountRequest {

  @NotBlank
  private String customerId;
  @NotNull
  @NumberFormat(style = Style.CURRENCY)
  private String initialBalance;

  public AccountRequest() {
  }

  private AccountRequest(Builder builder) {
    setCustomerId(builder.customerId);
    setInitialBalance(builder.initialBalance);
  }

  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public String getInitialBalance() {
    return initialBalance;
  }

  public void setInitialBalance(String initialBalance) {
    this.initialBalance = initialBalance;
  }

  public static final class Builder {

    private String customerId;
    private String initialBalance;

    public Builder() {
    }

    public Builder withCustomerId(String customerId) {
      this.customerId = customerId;
      return this;
    }

    public Builder withInitialBalance(String initialBalance) {
      this.initialBalance = initialBalance;
      return this;
    }

    public AccountRequest build() {
      return new AccountRequest(this);
    }
  }
}

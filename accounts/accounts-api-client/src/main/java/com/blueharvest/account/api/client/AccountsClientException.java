package com.blueharvest.account.api.client;

public class AccountsClientException extends Exception {

  private static final String CLIENT_ERROR_MESSAGE = "An error occurred invoking the accounts-api with response code: %d and message %s";

  public AccountsClientException(int code, String message) {
    super(String.format(CLIENT_ERROR_MESSAGE, code, message));
  }

  public AccountsClientException(Throwable parent) {
    super(parent);
  }

}

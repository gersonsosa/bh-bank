package com.blueharvest.account.api.client;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

@Component
public class AccountsApiClient {

  private final AccountsRetrofitClient accountsRetrofitClient;

  public AccountsApiClient(@Value("${accounts.api.url}") String accountsApiUrl) {
    final Retrofit retrofitClient = new Builder().baseUrl(accountsApiUrl).addConverterFactory(
        GsonConverterFactory.create()).build();
    this.accountsRetrofitClient = retrofitClient.create(AccountsRetrofitClient.class);
  }

  public boolean exists(String accountId) throws AccountsClientException {
    try {
      final Response<Void> response = accountsRetrofitClient.exists(accountId).execute();
      return response.isSuccessful();
    } catch (IOException e) {
      throw new AccountsClientException(e);
    }
  }
}

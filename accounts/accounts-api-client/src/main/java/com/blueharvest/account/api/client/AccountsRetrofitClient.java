package com.blueharvest.account.api.client;

import retrofit2.Call;
import retrofit2.http.HEAD;
import retrofit2.http.Path;

public interface AccountsRetrofitClient {

  @HEAD("accounts/{accountId}")
  Call<Void> exists(@Path("accountId") String accountId);
}

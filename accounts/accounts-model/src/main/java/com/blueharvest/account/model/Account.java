package com.blueharvest.account.model;

import com.blueharvest.transaction.model.Transaction;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class Account implements Serializable {

  private String id;
  private BigDecimal balance;
  private String customerId;
  private LocalDateTime created;
  private List<Transaction> transactions;

  public Account() {
  }

  public Account(String id, BigDecimal balance, String customerId, LocalDateTime created,
      List<Transaction> transactions) {
    this.id = id;
    this.balance = balance;
    this.customerId = customerId;
    this.created = created;
    this.transactions = transactions;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public LocalDateTime getCreated() {
    return created;
  }

  public void setCreated(LocalDateTime created) {
    this.created = created;
  }

  public List<Transaction> getTransactions() {
    return transactions;
  }

  public void setTransactions(List<Transaction> transactions) {
    this.transactions = transactions;
  }
}

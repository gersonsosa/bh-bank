package com.blueharvest.account.service.impl;

import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.blueharvest.account.model.Account;
import com.blueharvest.account.repository.AccountRepository;
import com.blueharvest.account.repository.CustomerRepository;
import com.blueharvest.account.service.AccountCreationException;
import com.blueharvest.account.service.AccountMessageSender;
import com.blueharvest.account.service.AccountsService;
import com.blueharvest.account.service.TransactionService;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.stream.Stream;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class AccountsServiceImplTest {

  private AccountsService subject;
  @Mock
  private Clock clock;
  @Mock
  private CustomerRepository customerRepository;
  @Mock
  private AccountRepository accountRepository;
  @Mock
  private TransactionService transactionService;
  @Mock
  private AccountMessageSender accountMessageSender;
  private LocalDateTime now;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new AccountsServiceImpl(clock, customerRepository, accountRepository,
        transactionService, accountMessageSender);

    final Instant instant = Instant.now();
    final ZoneId zoneId = ZoneId.systemDefault();
    this.now = LocalDateTime.ofInstant(instant, zoneId);

    when(clock.instant()).thenReturn(instant);
    when(clock.getZone()).thenReturn(zoneId);
  }

  @ParameterizedTest
  @MethodSource("accountRequestProvider")
  void shouldThrowIllegalArgumentExceptionIfCustomerDoesNotExists(String customerId,
      BigDecimal initialBalance) throws AccountCreationException {
    when(customerRepository.exists(customerId)).thenReturn(false);

    assertThatExceptionOfType(IllegalArgumentException.class)
        .isThrownBy(() -> subject.create(customerId, initialBalance));
  }

  @ParameterizedTest
  @MethodSource("accountRequestProvider")
  void shouldCreateAccount(String customerId, BigDecimal initialBalance, String accountId)
      throws AccountCreationException {
    final Account expected = createSampleAccount(accountId, initialBalance, now, customerId);
    when(customerRepository.exists(customerId)).thenReturn(true);
    when(accountRepository.save(refEq(expected, "id"))).thenReturn(expected);

    final Account account = subject.create(customerId, initialBalance);

    assertThat(account).isEqualToComparingFieldByField(expected);
  }

  @ParameterizedTest
  @ValueSource(longs = {1000L, -100L, 50L})
  void shouldCreateNewTransactionIfInitialBalanceIsNotZero(Long initialBalance)
      throws AccountCreationException {
    final String customerId = randomUUID().toString();
    final BigDecimal bigDecimalBalance = new BigDecimal(initialBalance);
    final String accountId = randomUUID().toString();

    final Account expected = createSampleAccount(accountId, bigDecimalBalance, now, customerId);
    when(customerRepository.exists(customerId)).thenReturn(true);
    when(accountRepository.save(refEq(expected, "id"))).thenReturn(expected);

    subject.create(customerId, bigDecimalBalance);

    verify(transactionService)
        .create(accountId, bigDecimalBalance, AccountsServiceImpl.INITIAL_TRANSACTION_DESCRIPTION);
  }

  @ParameterizedTest
  @MethodSource("accountRequestProvider")
  void shouldInvokeMessageQueueWhenAccountIsCreated(String customerId, BigDecimal initialBalance,
      String accountId) throws AccountCreationException {
    final Account account = createSampleAccount(accountId, initialBalance, now, customerId);
    when(customerRepository.exists(customerId)).thenReturn(true);
    when(accountRepository.save(refEq(account, "id"))).thenReturn(account);

    subject.create(customerId, initialBalance);

    verify(accountMessageSender).sendAccountCreated(account);
  }

  @ParameterizedTest
  @ValueSource(longs = {1000L, -100L, 50L, 0L})
  void shouldThrowAccountCreationExceptionWhenCallToCustomerRepositoryFails(Long initialBalance)
      throws AccountCreationException {
    final String customerId = randomUUID().toString();
    when(customerRepository.exists(customerId))
        .thenThrow(new AccountCreationException(new Exception()));

    assertThatExceptionOfType(AccountCreationException.class)
        .isThrownBy(() -> subject.create(customerId, new BigDecimal(initialBalance)));
  }

  @ParameterizedTest
  @MethodSource("accountRequestProvider")
  void findByIdShouldRetrieveAccountById(String accountId, BigDecimal initialBalance,
      String customerId) {
    Account expected = createSampleAccount(accountId, initialBalance, now, customerId);
    when(accountRepository.findById(accountId)).thenReturn(Optional.of(expected));

    final Optional<Account> accounts = subject.findById(accountId);

    assertThat(accounts).hasValue(expected);
  }

  private Account createSampleAccount(String accountId, BigDecimal amount,
      LocalDateTime now, String customerId) {
    final Account account = new Account();
    account.setId(accountId);
    account.setBalance(amount);
    account.setCreated(now);
    account.setCustomerId(customerId);
    return account;
  }

  private static Stream<Arguments> accountRequestProvider() {
    return Stream.of(
        Arguments.of(randomUUID().toString(), new BigDecimal(0L), randomUUID().toString(),
            RandomStringUtils.randomAlphabetic(255)),
        Arguments.of(randomUUID().toString(), new BigDecimal(100L), randomUUID().toString(),
            RandomStringUtils.randomAlphabetic(255)),
        Arguments.of(randomUUID().toString(), new BigDecimal(-100L), randomUUID().toString(),
            RandomStringUtils.randomAlphabetic(255))
    );
  }

}
package com.blueharvest.account.service;

import java.math.BigDecimal;

public interface TransactionService {

  /**
   * Sends a message to create a transaction with the given amount in the account
   *
   * @param accountId the account unique identifier
   * @param amount the amount of the transaction
   * @param description the description of the transaction
   */
  void create(String accountId, BigDecimal amount, String description);

}

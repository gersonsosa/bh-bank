package com.blueharvest.account.repository;

import com.blueharvest.account.service.AccountCreationException;

public interface CustomerRepository {

  /**
   * Returns whether the customer with the given id exists or not
   *
   * @param customerId the id of the customer to check for existence
   * @return true if the cusotmer exists, false otherwise
   */
  boolean exists(String customerId) throws AccountCreationException;
}

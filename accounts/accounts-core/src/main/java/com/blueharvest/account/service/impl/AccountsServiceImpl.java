package com.blueharvest.account.service.impl;

import com.blueharvest.account.model.Account;
import com.blueharvest.account.repository.AccountRepository;
import com.blueharvest.account.repository.CustomerRepository;
import com.blueharvest.account.service.AccountCreationException;
import com.blueharvest.account.service.AccountMessageSender;
import com.blueharvest.account.service.AccountsService;
import com.blueharvest.account.service.TransactionService;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Optional;

public class AccountsServiceImpl implements AccountsService {

  static final String INITIAL_TRANSACTION_DESCRIPTION = "Account initial balance";

  private final Clock clock;
  private final CustomerRepository customerRepository;
  private final AccountRepository accountRepository;
  private final TransactionService transactionService;
  private final AccountMessageSender accountMessageSender;

  public AccountsServiceImpl(Clock clock,
      CustomerRepository customerRepository,
      AccountRepository accountRepository,
      TransactionService transactionService,
      AccountMessageSender accountMessageSender) {
    this.clock = clock;
    this.customerRepository = customerRepository;
    this.accountRepository = accountRepository;
    this.transactionService = transactionService;
    this.accountMessageSender = accountMessageSender;
  }

  @Override
  public Account create(String customerId, BigDecimal initialBalance)
      throws AccountCreationException {
    if (!customerRepository.exists(customerId)) {
      throw new IllegalArgumentException(
          String.format("Customer with id %s doesn't exists", customerId));
    }
    final Account account = new Account();
    account.setCustomerId(customerId);
    account.setBalance(initialBalance);
    account.setCreated(LocalDateTime.now(clock));

    final Account saved = accountRepository.save(account);
    accountMessageSender.sendAccountCreated(saved);

    if (initialBalance.compareTo(BigDecimal.ZERO) != 0) {
      transactionService.create(saved.getId(), initialBalance, INITIAL_TRANSACTION_DESCRIPTION);
    }
    return saved;
  }

  @Override
  public Optional<Account> findById(String accountId) {
    return accountRepository.findById(accountId);
  }
}

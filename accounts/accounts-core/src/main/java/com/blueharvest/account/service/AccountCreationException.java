package com.blueharvest.account.service;

public class AccountCreationException extends Exception {

  public AccountCreationException(Throwable throwable) {
    super(throwable);
  }
}

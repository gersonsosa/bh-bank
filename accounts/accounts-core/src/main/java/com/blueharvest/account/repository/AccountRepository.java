package com.blueharvest.account.repository;

import com.blueharvest.account.model.Account;
import java.util.Optional;

public interface AccountRepository {

  /**
   * Persists an account
   *
   * @param account the account to be persisted
   * @return the account with the generated identifier
   */
  Account save(Account account);

  /**
   * Retrieves an account by id
   * @param accountId the account unique identifier
   * @return the account or an {@link Optional#empty()}
   */
  Optional<Account> findById(String accountId);
}

package com.blueharvest.account.service;

import com.blueharvest.account.model.Account;

public interface AccountMessageSender {

  void sendAccountCreated(Account account);

}

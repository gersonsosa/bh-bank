package com.blueharvest.account.service;

import com.blueharvest.account.model.Account;
import java.math.BigDecimal;
import java.util.Optional;

public interface AccountsService {

  /**
   * Creates an account for an already existing customer
   *
   * @param customerId the customer unique id
   * @param initialBalance the initial balance of the new account, if != 0 a transaction is issued
   * for the new account
   * @return the new account created
   */
  Account create(String customerId, BigDecimal initialBalance) throws AccountCreationException;

  /**
   * Retrieves the account for the given id
   *
   * @param accountId the account unique identifier
   */
  Optional<Account> findById(String accountId);

}

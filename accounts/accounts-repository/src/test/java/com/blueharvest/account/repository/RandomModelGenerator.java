package com.blueharvest.account.repository;

import static java.util.UUID.randomUUID;

import com.blueharvest.account.model.Account;
import com.blueharvest.account.repository.jpa.model.AccountEntity;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.UUID;
import org.apache.commons.lang3.RandomUtils;

public class RandomModelGenerator {

  private final static LocalDateTime now = LocalDateTime.now();

  public static AccountEntity createRandomAccountEntity() {
    final AccountEntity entity = createRandomAccountEntityWithoutId();
    entity.setId(UUID.randomUUID().toString());
    return entity;
  }

  public static AccountEntity createRandomAccountEntityWithoutId() {
    final AccountEntity entity = new AccountEntity();
    entity.setCustomerId(UUID.randomUUID().toString());
    entity.setBalance(new BigDecimal(RandomUtils.nextLong()));
    entity.setCreated(now);
    return entity;
  }

  public static Account createRandomAccount() {
    return new Account(null, new BigDecimal(RandomUtils.nextFloat()),
        randomUUID().toString(), now, Collections.emptyList());
  }

}

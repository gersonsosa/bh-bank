package com.blueharvest.account.repository.impl;

import static com.blueharvest.account.repository.RandomModelGenerator.createRandomAccount;
import static com.blueharvest.account.repository.RandomModelGenerator.createRandomAccountEntity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.blueharvest.account.model.Account;
import com.blueharvest.account.repository.AccountRepository;
import com.blueharvest.account.repository.jpa.AccountJpaRepository;
import com.blueharvest.account.repository.jpa.model.AccountEntity;
import com.blueharvest.account.repository.mapper.AccountMapper;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class AccountRepositoryImplTest {

  private AccountRepository subject;
  @Mock
  private AccountJpaRepository accountJpaRepository;
  @Mock
  private AccountMapper accountMapper;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    subject = new AccountRepositoryImpl(accountJpaRepository, accountMapper);
  }

  @Test
  void shouldSaveAccount() {
    final Account sourceAccount = createRandomAccount();
    final AccountEntity entity = createRandomAccountEntity();

    when(accountMapper.map(sourceAccount)).thenReturn(entity);
    when(accountJpaRepository.save(entity)).thenReturn(entity);
    when(accountMapper.map(entity)).thenReturn(sourceAccount);

    final Account accountResult = subject.save(sourceAccount);

    assertThat(accountResult).isEqualToComparingFieldByField(sourceAccount);
  }

  @Test
  void shouldReturnAccountsById() {
    final AccountEntity entity = createRandomAccountEntity();
    final Account expected = createRandomAccount();
    when(accountJpaRepository.findById(entity.getCustomerId())).thenReturn(Optional.of(entity));
    when(accountMapper.map(entity)).thenReturn(expected);

    final Optional<Account> accounts = subject.findById(entity.getCustomerId());

    assertThat(accounts).hasValueSatisfying(
        account -> assertThat(account).isEqualToIgnoringGivenFields(expected, "transactions"));
  }
}
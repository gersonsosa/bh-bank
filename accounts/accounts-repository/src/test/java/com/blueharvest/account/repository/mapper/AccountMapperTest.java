package com.blueharvest.account.repository.mapper;

import static java.util.UUID.randomUUID;
import static org.apache.commons.lang3.RandomUtils.nextLong;
import static org.assertj.core.api.Assertions.assertThat;

import com.blueharvest.account.model.Account;
import com.blueharvest.account.repository.jpa.model.AccountEntity;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class AccountMapperTest {

  private AccountMapper subject;

  @BeforeEach
  void setUp() {
    subject = new AccountMapper();
  }

  @ParameterizedTest
  @MethodSource("accountProvider")
  void shouldMapAccountToAccountEntity(String accountId, String customerId,
      BigDecimal balance, LocalDateTime created) {
    final Account account = new Account(accountId, balance, customerId, created,
        Collections.emptyList());
    final AccountEntity entity = subject.map(account);

    assertThat(entity).isEqualToComparingFieldByField(account);
  }

  @ParameterizedTest
  @MethodSource("accountProvider")
  void shouldMapAccountEntityToAccount(String accountId, String customerId,
      BigDecimal balance, LocalDateTime created) {
    final AccountEntity accountEntity = createAccountEntity(accountId, customerId, balance,
        created);
    final Account account = subject.map(accountEntity);

    assertThat(account).isEqualToIgnoringGivenFields(accountEntity, "transactions");
  }

  @ParameterizedTest
  @MethodSource("accountProvider")
  void shouldMapListOfAccountEntitiesToListOfAccounts(String accountId, String customerId,
      BigDecimal balance, LocalDateTime created) {
    final AccountEntity accountEntity = createAccountEntity(accountId, customerId, balance,
        created);
    final List<AccountEntity> entities = Collections.singletonList(accountEntity);
    final List<Account> accounts = subject.map(entities);

    assertThat(accounts).usingElementComparatorIgnoringFields("transactions").isEqualTo(entities);
  }

  private AccountEntity createAccountEntity(String accountId, String customerId,
      BigDecimal balance, LocalDateTime created) {
    final AccountEntity accountEntity = new AccountEntity();
    accountEntity.setId(accountId);
    accountEntity.setCustomerId(customerId);
    accountEntity.setBalance(balance);
    accountEntity.setCreated(created);
    return accountEntity;
  }

  private static Stream<Arguments> accountProvider() {
    return Stream.of(
        Arguments.of(randomUUID().toString(), randomUUID().toString(), new BigDecimal(nextLong()),
            LocalDateTime.now()),
        Arguments.of(randomUUID().toString(), randomUUID().toString(), new BigDecimal(1000L),
            LocalDateTime.now()),
        Arguments.of(randomUUID().toString(), randomUUID().toString(), new BigDecimal(-1000L),
            LocalDateTime.now()),
        Arguments.of(randomUUID().toString(), randomUUID().toString(), new BigDecimal(0L),
            LocalDateTime.now())
    );
  }
}
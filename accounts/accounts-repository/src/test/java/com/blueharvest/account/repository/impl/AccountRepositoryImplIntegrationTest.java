package com.blueharvest.account.repository.impl;

import static com.blueharvest.account.repository.RandomModelGenerator.createRandomAccount;
import static com.blueharvest.account.repository.RandomModelGenerator.createRandomAccountEntityWithoutId;
import static org.assertj.core.api.Assertions.assertThat;

import com.blueharvest.account.model.Account;
import com.blueharvest.account.repository.TestRepositoryConfig;
import com.blueharvest.account.repository.jpa.AccountJpaRepository;
import com.blueharvest.account.repository.jpa.model.AccountEntity;
import java.util.Optional;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@ContextConfiguration(classes = TestRepositoryConfig.class)
@Tag("integration")
class AccountRepositoryImplIntegrationTest {

  @Autowired
  private AccountRepositoryImpl subject;

  @Autowired
  private TestEntityManager testEntityManager;

  @Autowired
  private AccountJpaRepository accountJpaRepository;

  @Test
  void saveShouldSaveAccountToDatabase() {
    final Account saved = subject.save(createRandomAccount());

    final Optional<AccountEntity> entityOptional = accountJpaRepository.findById(saved.getId());

    assertThat(entityOptional).hasValueSatisfying(
        accountEntity -> assertThat(accountEntity).isEqualToComparingFieldByField(saved));
  }

  @Test
  void findByIdShouldRetrieveAccountFromDatabase() {
    final AccountEntity entity = createRandomAccountEntityWithoutId();
    final AccountEntity persisted = testEntityManager.persist(entity);

    final Optional<Account> accountOptional = subject.findById(persisted.getId());

    assertThat(accountOptional).hasValueSatisfying(
        account -> assertThat(account).isEqualToIgnoringGivenFields(persisted, "transactions"));
  }
}
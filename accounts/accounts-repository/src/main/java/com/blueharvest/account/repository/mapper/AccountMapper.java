package com.blueharvest.account.repository.mapper;

import static com.googlecode.jmapper.api.JMapperAPI.mappedClass;

import com.blueharvest.account.model.Account;
import com.blueharvest.account.repository.jpa.model.AccountEntity;
import com.googlecode.jmapper.JMapper;
import com.googlecode.jmapper.api.Global;
import com.googlecode.jmapper.api.JMapperAPI;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class AccountMapper {

  private final JMapper<AccountEntity, Account> accountJMapper;
  private final JMapper<Account, AccountEntity> accountEntityJMapper;

  AccountMapper() {
    JMapperAPI jMapperAPI = new JMapperAPI()
        .add(mappedClass(Account.class).add(new Global().excludedAttributes("transactions")))
        .add(mappedClass(AccountEntity.class).add(new Global()));
    this.accountJMapper = new JMapper<>(AccountEntity.class, Account.class, jMapperAPI);
    this.accountEntityJMapper = new JMapper<>(Account.class, AccountEntity.class, jMapperAPI);
  }

  public AccountEntity map(Account account) {
    return accountJMapper.getDestination(account);
  }

  public Account map(AccountEntity entity) {
    return accountEntityJMapper.getDestination(entity);
  }

  List<Account> map(List<AccountEntity> entities) {
    return entities.parallelStream().map(this::map).collect(Collectors.toList());
  }
}

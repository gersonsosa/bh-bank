package com.blueharvest.account.repository.jpa;

import com.blueharvest.account.repository.jpa.model.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountJpaRepository extends JpaRepository<AccountEntity, String> {
}

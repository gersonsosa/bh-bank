package com.blueharvest.account.repository.impl;

import com.blueharvest.account.model.Account;
import com.blueharvest.account.repository.AccountRepository;
import com.blueharvest.account.repository.jpa.AccountJpaRepository;
import com.blueharvest.account.repository.mapper.AccountMapper;
import java.util.Optional;
import org.springframework.stereotype.Repository;

@Repository
public class AccountRepositoryImpl implements AccountRepository {

  private final AccountJpaRepository accountJpaRepository;
  private final AccountMapper accountMapper;

  AccountRepositoryImpl(
      AccountJpaRepository accountJpaRepository,
      AccountMapper accountMapper) {
    this.accountJpaRepository = accountJpaRepository;
    this.accountMapper = accountMapper;
  }

  @Override
  public Account save(Account account) {
    return accountMapper.map(accountJpaRepository.save(accountMapper.map(account)));
  }

  @Override
  public Optional<Account> findById(String customerId) {
    return accountJpaRepository.findById(customerId).map(accountMapper::map);
  }
}

# bh bank
Blue Harvest Code challenge accounts and transactions application

# Introduction
The application allows to create bank accounts for existing customers in the database:

A sample body request to create the account:
```
{
	"customerId": "9938a1ea-f1de-4de5-a5f6-07ed1d763db7",
	"initialBalance": "100"
}
```

If the initialBalance parameter is different than zero the application also creates a transaction with the value given.

A few customers are inserted using a sql script when the customer application boots.

A get request to the customer api http://<ip addr>/customers/<customer-id> retrieves all accounts and
transactions for the given customer id.

Single transactions can also be created using the payload

```
{
	"accountId": "9938a1ea-f1de-4de5-a5f6-07ed1d763db7",
	"amount": "100",
	"description": "Payment ..."
}
```

All possible operations are visible using swagger api enabled in all microservices.

# Architecture
The application is composed of 4 microservices and a message broker:
- customers-api: handles operations related to the customer (currently only retrieving customer, accounts and transactions data)
- account-api: handles operations related to the account (create for an existing customer, retrieve account data)
- transactions-api: handles operations related to the transactions (create transactions, query by account id)
- account-view-api: a materialized view api of the accounts and transactions
- message-broker: used to send messages from account-api and transactions api to the account-view-api when an account or transactions are created.

All the applications are build using concepts of [clean architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) these are basically S.O.L.I.D principles using concepts of OOP, the applications are built in a way that the web framework, the repository, and other frameworks are detached from the core functionality of the applications this ensures that any change of framework or technology doesn't affect the whole application but only the relevant part.

Individual microservices are separated in the following modules:
- entity / model, business rules data structures
- core, business, contains business logic or use cases, is the core and doesn't depend on frameworks or the database
- repository, contains all database related glue code
- rest-api, contains all rest api and message broker implementations, also is the main application launcher

Using this structure allows to reuse modules when needed and using simple jar dependency

# Challenges

The main challenge was to separate the account and transaction services, in a monolothic application
the concepts are tightly coupled since there is a relationship many-to-many between transactions and accounts, 
returning the list of accounts for a customer with the transactions is trivial in an unique database.
However since the services are separated and therefore each has its own database, 
there are two possible ways to retrieve the required data, one is using a synchronous call to the 
corresponding services and the other is maintain a read-only cache of the data updated in real time 
the latter allows the application to be more scalable, doing a many-to-many join in memory can be costly 
and take O(n*m) where n is the number od accounts and m the number of transactions.

In order to solve this, a materialized view pattern was used a new service was created to keep the
information of the account and the transactions in a read-only database, whenever an account or transaction
is created a message is sent to the account-view-api through a message broker and this application
updates its cache in real time.

When a query to the customer api has requested an invocation to the account-view-api is done in order to retrieve all the account with the corresponding transactions.

Another challenge was validate that the customer exists of an account exists before creating an account or transaction, this was solved using synchronous calls to the corresponding api's since in this case the calls doesn't represent a performance issue.

# Deploy
There are a couple of ways to deploy the application

Docker images are build every time the project is compiled using maven command:
`mvn clean install`
These images are created locally and can be pushed to a docker hub account.
The latest version of the images is also published in my [docker hub](https://hub.docker.com/u/hghar)

All configuration is done using environment variables which allows to manage it in different environments efficiently.
See deployment descriptors for details on needed configuration.

## Kuebernetes
Under bh-bank/deployment folder there are service, deployments and configmaps descriptors necessary to deploy the application to a kubernetes cluster:
1. Install rabbitmq in the cluster using the default configuration helm chart with command 
  `helm install --name my-rabbit --set rabbitmq.username=guest,rabbitmq.password=guest stable/rabbitmq`
2. Deploy application usgin `kubectl create -f ./` in the bh-bank/deployment directory, this will deploy a single instance of each service and expose the customer and accounts apis.
For the UI
3. Set kubernetes exposed Ip addresses for Customers-api and Accounts-api services in the file `bh-bank-ui/src/environments/environment.ts`
4. Compile the docker image for the application using `docker build -f Dockerfile.comp -t hghar/bh-bank-ui-comp .`

Note. All the microservices are using an in memory database automatically configured by Spring-boot,
so scaling the application will lead to unexpected results (data inconsistency) since the database
runs inside the same java process as the spring boot service.

## Docker compose
1. Build the images using command `mvn clean install` in the root folder of the project
2. Compile the docker image for the application using `docker build -f Dockerfile.comp -t hghar/bh-bank-ui-comp .`
3. A provided docker-compose.yml file is provided to run the application services in a local machine.

# Improvements
There is room for various improvements,

- An easier way to deploy such as a helm chart + ci cd + aws eks
- Better implementation of a message broker

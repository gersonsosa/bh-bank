export const environment = {
  production: true,
  customersUrl: "http://customers-api/customers",
  accountsUrl: "http://accounts-api/accounts"
};

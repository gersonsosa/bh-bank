import { Injectable } from '@angular/core';
import { Customer } from './customers/customer';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private customersUrl: string;

  constructor(private http: HttpClient) {
    this.customersUrl = environment.customersUrl;
  }

  getCustomers(): Observable<Customer[]> {
    return this.http.get<Customer[]>(this.customersUrl).pipe(
      catchError(this.handleError<Customer[]>("getCustomers", []))
    );
  }

  getCustomerDetail(customerId: string) {
    const customerUrl = `${this.customersUrl}/${customerId}`;
    return this.http.get<Customer>(customerUrl).pipe(
      catchError(this.handleError<Customer>(`getCustomer id=${customerId}`))
    );
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: show error to user

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

export interface Transaction {
    id: string;
    amount: number;
    description: string;
    created: string;
}
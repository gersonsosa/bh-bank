import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionsComponent } from './transactions.component';
import { MatListModule, MatIconModule } from '@angular/material';
import { Component } from '@angular/core';
import { Account } from '../accounts/account';

describe('TransactionsComponent', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestHostComponent, TransactionsComponent ],
      imports: [MatIconModule, MatListModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  @Component({
    selector: `host-component`,
    template: `<app-transactions account="${{id: '', balance: 0, created: '', transactions: []}}"></app-transactions>`
  })
  class TestHostComponent {
  }
});

import { Component, OnInit, Input } from '@angular/core';
import { Transaction } from './transaction';
import { Account } from '../accounts/account';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

  @Input() account: Account;

  constructor() { }

  ngOnInit() {
  }
}

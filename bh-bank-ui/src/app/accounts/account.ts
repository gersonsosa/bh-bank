import { Transaction } from '../transactions/transaction';

export interface Account {
    id: string;
    balance: number;
    created: string;
    transactions: Transaction[];
}
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { AccountService, AccountRequest } from '../account.service';
import { Account } from './account';
import { CustomerService } from '../customer.service';
import { Customer } from '../customers/customer';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {

  account: Account;
  newAccount: boolean = false;
  accounts: Account[];
  customerId: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private customerService: CustomerService,
    private accountService: AccountService
  ) { }

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.loadCustomerDetails(params.get('customerId')))
    ).subscribe(customer => {
      this.accounts = customer.accounts;
      this.customerId = customer.id;
    });
  }

  loadCustomerDetails(customerId: string): Observable<Customer> {
    return this.customerService.getCustomerDetail(customerId);
  }

  createAccountToggle() {
    this.newAccount = !this.newAccount;
  }

  createAccount(initialBalance: string) {
    this.createAccountToggle();
    if (!initialBalance) { return; }
    let initialBalanceNumber = +initialBalance;
    let accountRequest: AccountRequest = { customerId: this.customerId, initialBalance: initialBalanceNumber };
    this.accountService.createAccount(accountRequest).subscribe(newAccount => this.accounts.push(newAccount));
  }

  onSelect(account: Account) {
    this.account = account;
  }

}

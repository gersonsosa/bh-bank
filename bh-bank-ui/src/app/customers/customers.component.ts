import { Component, OnInit } from '@angular/core';
import { Customer } from './customer';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {

  customers: Customer[];

  customer: Customer;

  constructor(private service: CustomerService) { }

  ngOnInit() {
    this.service.getCustomers()
      .subscribe(customers => this.customers = customers);
  }

}

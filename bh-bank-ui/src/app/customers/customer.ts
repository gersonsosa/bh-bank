import { Account } from '../accounts/account';

export interface Customer {
    id: string;
    name: string;
    surname: string;
    accounts: Account[];
}
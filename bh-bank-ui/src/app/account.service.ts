import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Account } from './accounts/account';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { tap, catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

export interface AccountRequest {
  customerId: string,
  initialBalance: number
}

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private accountsUrl: string;

  constructor(private http: HttpClient) {
    this.accountsUrl = environment.accountsUrl;
  }

  createAccount(accountRequest: AccountRequest): Observable<Account> {
    return this.http.post<Account>(this.accountsUrl, accountRequest, httpOptions).pipe(
      tap((newAccount: Account) => this.log(`account added: ${newAccount}`)),
      catchError(this.handleError<Account>('createAccount'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  log(message: string): any {
    console.log("Account service: " + message)
  }
}
